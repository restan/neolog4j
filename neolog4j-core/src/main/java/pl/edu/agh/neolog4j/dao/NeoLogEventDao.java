package pl.edu.agh.neolog4j.dao;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.conversion.EndResult;
import org.springframework.data.neo4j.conversion.Result;
import org.springframework.stereotype.Service;

import pl.edu.agh.neolog4j.dao.query.LogEventQuery;
import pl.edu.agh.neolog4j.dao.repository.NeoLogEventRepository;
import pl.edu.agh.neolog4j.model.ApplicationInstance;
import pl.edu.agh.neolog4j.model.NeoLogEvent;
import pl.edu.agh.neolog4j.model.SimpleLogEvent;
import pl.edu.agh.neolog4j.model.SourceClass;

@Service
public class NeoLogEventDao extends AbstractDao {
	
	@Autowired protected SourceDao sourceDao;
	@Autowired protected EndpointDao endpointDao;
	@Autowired protected NeoLogEventRepository neoLogEventRepository;
	
	
	
	public NeoLogEvent getNeoLogEvent(SimpleLogEvent p_event, NeoLogEvent p_previous,
			ApplicationInstance p_applicationInstance){
		
		NeoLogEvent neoLogEvent = createNewNode(NeoLogEvent.class, null);
		fillNeoLogEventProperties(neoLogEvent, p_event, p_previous, p_applicationInstance);
		this.template.save(neoLogEvent);
		return neoLogEvent;
		
	}
	
	public NeoLogEvent getNeoLogEvent(SimpleLogEvent p_event, ApplicationInstance p_applicationInstance){
		
		NeoLogEvent neoLogEvent = createNewNode(NeoLogEvent.class, null);
		fillNeoLogEventProperties(neoLogEvent, p_event, p_applicationInstance);
		this.template.save(neoLogEvent);
		return neoLogEvent;
		
	}
	
	public NeoLogEvent getNeoLogEvent() {
		return createNewNode(NeoLogEvent.class, null);
	}
	
	protected void fillNeoLogEventProperties(NeoLogEvent p_neoLogEvent,
			SimpleLogEvent p_event, ApplicationInstance p_instance) {
		fillNeoLogEventProperties(p_neoLogEvent, p_event, null, p_instance);
	}
	
	protected void fillNeoLogEventProperties(NeoLogEvent p_neoLogEvent, SimpleLogEvent p_event,
			NeoLogEvent p_previous, ApplicationInstance p_applicationInstance) {
		
		if (p_event != null) {
			StackTraceElement stackTraceElement = p_event.getStackTraceElement();
			SourceClass source = null;
			if(stackTraceElement != null){
				String className = stackTraceElement.getClassName();
				source = this.sourceDao.getSource(className);
			}
			p_neoLogEvent.setLoggerName(p_event.getLoggerName());
			p_neoLogEvent.setLevel(p_event.getLevel());
			p_neoLogEvent.setThreadName(p_event.getThreadName());
			p_neoLogEvent.setTimestamp(p_event.getTimestamp());
			p_neoLogEvent.setSource(source);
			p_neoLogEvent.setMessage(p_event.getMessage());
			p_neoLogEvent.setApplicationInstance(p_applicationInstance);
		}
		p_neoLogEvent.setPrevious(p_previous);
		
	}
	
	public Set<NeoLogEvent> getNextEvents(NeoLogEvent p_event) {
		return iterableToSet(this.neoLogEventRepository.getNext(p_event));
	}
	
	public Set<NeoLogEvent> query(LogEventQuery p_query) {
		Result<Map<String, Object>> query = this.template
				.query(p_query.getCypherQueryString(), new HashMap<String, Object>());
		EndResult<NeoLogEvent> endResult = query.to(NeoLogEvent.class);
		return iterableToSet(endResult);
	}
	
	public void save(NeoLogEvent neoLogEvent){
		this.getTemplate().save(neoLogEvent);
	}
}
