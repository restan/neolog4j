package pl.edu.agh.neolog4j.model;

import java.util.Set;

import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

@NodeEntity
public class OutEndpoint extends Endpoint {
	
	@RelatedTo(type = RelationNames.OUT_ENDPOINT_DEST)
	private Set<InEndpoint> destinations;
	
	private OutEndpoint(){
		
	}

	public Set<InEndpoint> getDestinations() {
		return destinations;
	}

	public void setDestinations(Set<InEndpoint> destinations) {
		this.destinations = destinations;
	}

}
