package pl.edu.agh.neolog4j.model;

import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

@NodeEntity
public class SourcePackage extends AbstractModelClass implements UniqueNamedNodesInterface {
	
	
	public final static String SOURCE_PACKAGE_INDEX = "name";
	
	@Indexed(indexName = SOURCE_PACKAGE_INDEX, unique = true) 
	private String name;
	
	@RelatedTo(type = RelationNames.SOURCE_PACKAGE_PACKAGE)
	private SourcePackage parentPackage;
	
	private SourcePackage() {
		
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public SourcePackage getParentPackage() {
		return parentPackage;
	}
	
	public void setParentPackage(SourcePackage parentPackage) {
		this.parentPackage = parentPackage;
	}
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("SourcePackage [");
		sb.append("name=").append(this.name).append(", ");
		sb.append("parentPackage=").append(this.parentPackage != null ? this.parentPackage.toString() : null).append(", ");
		sb.append("]");
		return sb.toString();
	}
}
