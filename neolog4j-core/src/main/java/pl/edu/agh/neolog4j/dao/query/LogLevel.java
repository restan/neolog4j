package pl.edu.agh.neolog4j.dao.query;

public enum LogLevel {
	ERROR("ERROR"),
	WARNING("WARNING"),
	INFO("INFO"),
	DEBUG("DEBUG"),
	TRACE("TRACE");
	
	private String name;
	
	public String val(){
		return this.name;
	}
	
	private LogLevel(String name){
		this.name = name;
	}
}
