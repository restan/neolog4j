package pl.edu.agh.neolog4j.dao;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.neo4j.graphdb.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.edu.agh.neolog4j.dao.repository.ApplicationRepository;
import pl.edu.agh.neolog4j.model.Application;
import pl.edu.agh.neolog4j.model.ApplicationInstance;
import pl.edu.agh.neolog4j.model.Endpoint;

@Service
public class ApplicationDao extends AbstractDao {

	@Autowired 
	protected ApplicationRepository applicationRepository;
	
	public Application getApplication(String name){
		
		return getOrCreateNamedNode(name, Application.APPLICATION_INDEX, Application.class);
	}
	
	public ApplicationInstance getApplicationInstance(String appName){
		return getApplicationInstance(appName, new Date());
	}
	
	public ApplicationInstance getApplicationInstance(String appName, Date date) {
		Transaction tx = this.template.getGraphDatabase().beginTx();
		ApplicationInstance applicaitonInstance = null;
		try {			
			Application application = getApplication(appName);
			applicaitonInstance = createNewNode(ApplicationInstance.class, null);
			applicaitonInstance.setApplication(application);
			applicaitonInstance.setCreateDate(date);
			this.template.save(applicaitonInstance);
			tx.success();
		} finally {
			tx.finish();
		}
		return applicaitonInstance;
	}
	
	public Set<ApplicationInstance> getInstances(String p_appName, Date p_from) {
		
		Application application = getApplication(p_appName);
		long timestamp = 0;
		if (p_from != null) {			
			timestamp = p_from.getTime();
		}
		return iterableToSet(this.applicationRepository.getInstances(application, timestamp));
	}
	
	public Set<ApplicationInstance> getApplicationInstances(String p_appName, Set<Long> p_createTimestamps) {
		Application application = getApplication(p_appName);
		return iterableToSet(this.applicationRepository.getInstances(application, p_createTimestamps));
	}
	
	
	public Set<ApplicationInstance> getAllInstances(Application p_application) {
		return iterableToSet(this.applicationRepository.getInstances(p_application));
	}
	
	public Set<Endpoint> getApplicationEndpoints(Application p_application) {
		return iterableToSet(this.applicationRepository.getEndpoints(p_application));
	}
	
	public Set<Application> getApplications() {
		return getAllNodes(Application.class);
	}
	
	public ApplicationInstance getApplicationInstance(long graphId){
		return this.template.findOne(graphId, ApplicationInstance.class);
	}
	
}
