package pl.edu.agh.neolog4j.model;

import java.util.Set;

import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

@NodeEntity
public class InEndpoint extends Endpoint{
	
	@RelatedTo(type = RelationNames.IN_ENDPOINT_SOURCES)
	private Set<OutEndpoint> sources;
	
	private InEndpoint(){
		
	}

	public Set<OutEndpoint> getSources() {
		return sources;
	}

	public void setSources(Set<OutEndpoint> sources) {
		this.sources = sources;
	}

}
