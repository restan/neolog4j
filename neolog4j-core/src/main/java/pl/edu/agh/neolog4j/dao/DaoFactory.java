package pl.edu.agh.neolog4j.dao;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

@Service
public class DaoFactory {
	
	protected final static String NEO_LOG4J_DATABASE_URL_PROPERTY = "neoLog4j.db.url";
	protected final static String NEO_LOG4J_DATABASE_SERVER_USER = "neoLog4j.db.user";
	protected final static String NEO_LOG4J_DATABASE_SERVER_PASSWORD = "neoLog4j.db.password";
	protected static Map<DatabaseInstanceKey, DaoFactory> instances = new HashMap<DatabaseInstanceKey, DaoFactory>();
	
	@Autowired
	protected ApplicationDao applicationDao;
	@Autowired
	protected NeoLogEventDao neoLogEventDao;
	@Autowired
	protected SourceDao sourceDao;
	@Autowired
	protected EndpointDao endpointDao;
	
	protected String dbUrl;
	
	
	public static synchronized DaoFactory getInstance(String p_url) {
		return getInstance(p_url, null, null);
	}
	
	public static synchronized DaoFactory getInstance(String p_url, String p_user, String p_passwd) {
		DatabaseInstanceKey instanceKey = new DatabaseInstanceKey(p_url, p_user, p_passwd);
		if (!instances.containsKey(instanceKey)) {
			System.setProperty(NEO_LOG4J_DATABASE_URL_PROPERTY, p_url);
			if (p_user == null) {
				p_user = "";
			}
			if (p_passwd == null) {
				p_passwd = "";
			}
			System.setProperty(NEO_LOG4J_DATABASE_SERVER_USER, p_user);
			System.setProperty(NEO_LOG4J_DATABASE_SERVER_PASSWORD, p_passwd);
			ConfigurableApplicationContext context = new ClassPathXmlApplicationContext(
					"META-INF/spring/applicationContext.xml");
			DaoFactory daoFactory = (DaoFactory) context.getBean(DaoFactory.class);
			daoFactory.dbUrl = p_url;
			instances.put(instanceKey, daoFactory);
		}
		return instances.get(instanceKey);
	}
	
	public String getUrl() {
		return this.dbUrl;
	}
	
	public ApplicationDao getApplicationDao(){
		return this.applicationDao;
	}
	
	public NeoLogEventDao getNeoLogEventDao(){
		return this.neoLogEventDao;
	}
	
	public SourceDao getSourceDao(){
		return this.sourceDao;
	}
	
	public EndpointDao getEndpointDao() {
		return this.endpointDao;
	}
	
	public static class DatabaseInstanceKey	{
		private String url = null;
		private String user = null;
		private String passwd = null;
		
		public DatabaseInstanceKey(String p_url, String p_user, String p_passwd) {
			this.url = p_url;
			this.user = p_user;
			this.passwd = p_passwd;
		}
		
		public DatabaseInstanceKey(String p_url) {
			this(p_url, null, null);
		}
		@Override
		public boolean equals(Object arg0) {
			if (arg0 instanceof DatabaseInstanceKey) {
				DatabaseInstanceKey other = (DatabaseInstanceKey) arg0;
				if (this.url.equals(other.url)) {
					if (this.user != null) {
						return this.user.equals(other.user)
								&& this.passwd.equals(other.passwd);
					}
				}
			}
			return false;
		}
		
		@Override
		public int hashCode() {
			int hashCode = this.url.hashCode();
			if (this.user != null) {
				hashCode = hashCode ^ this.user.hashCode();
				hashCode = hashCode ^ this.passwd.hashCode();
			}
			return hashCode;
		}
	}
	
}
