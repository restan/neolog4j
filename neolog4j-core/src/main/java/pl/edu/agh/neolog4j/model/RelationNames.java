package pl.edu.agh.neolog4j.model;

public class RelationNames {

	public final static String LOG_EVENT_SOURCE = "EVENT_SOURCE";
	public final static String LOG_EVENT_SOURCE_ENDPOINT = "SOURCE_ENDPOINT";
	public final static String LOG_EVENT_DESTINATION_ENDPOINT = "DESTINATION_ENDPOINT";
	public final static String LOG_EVENT_APPLICATION_INSTANCE = "APPLICATION_INSTANCE";
	public final static String LOG_EVENT_PREVIOUS = "PREVIOUS";
	
	public final static String SOURCE_CLASS_PACKAGE = "PACKAGE";
	public final static String SOURCE_PACKAGE_PACKAGE = "PARENT_PACKAGE";
	
	public final static String APPLICATION_INSTANCE_APPLICATION = "INSTANCE_OF";
	
	public final static String ENDPOINT_APPLICATION = "APPLICATION";
	public final static String IN_ENDPOINT_SOURCES = "ENDPOINT_SOURCE";
	public final static String OUT_ENDPOINT_DEST = "DESTINATION";
	
	
	
}
