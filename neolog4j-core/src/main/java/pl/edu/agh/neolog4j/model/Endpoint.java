package pl.edu.agh.neolog4j.model;

import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

@NodeEntity
public abstract class Endpoint extends AbstractModelClass implements UniqueNamedNodesInterface {
	
	public final static String ENDPOINT_INDEX = "name";
	
	
	@Indexed(indexName = ENDPOINT_INDEX, unique = true) 
	private String name;
	
	@RelatedTo(type = RelationNames.ENDPOINT_APPLICATION)
	@Fetch
	private Application application;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}
	
}
