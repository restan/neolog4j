package pl.edu.agh.neolog4j.model;

import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;

@NodeEntity
public class Application extends AbstractModelClass implements UniqueNamedNodesInterface {
	
	public final static String APPLICATION_INDEX = "name";
	
	@Indexed(indexName = APPLICATION_INDEX, unique = true)
	private String name;
	
	public Application() {
		
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("Application [");
		sb.append("name=").append(this.name).append(", ");
		sb.append("]");
		return sb.toString();
	}
}
