package pl.edu.agh.neolog4j.model;

import java.util.Date;

import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

@NodeEntity
public class ApplicationInstance extends AbstractModelClass implements Comparable<ApplicationInstance> {
	
	@RelatedTo(type = RelationNames.APPLICATION_INSTANCE_APPLICATION)
	@Fetch
	private Application application;
	
	private Long createDateTimestamp;
	
	public ApplicationInstance(){
		
	}

	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}

	public Date getCreateDate() {
		return new Date(this.createDateTimestamp);
	}

	public void setCreateDate(Date createDate) {
		this.createDateTimestamp = createDate.getTime();
	}
	
	public Long getCreateTimestamp() {
		return this.createDateTimestamp;
	}
	
	public int compareTo(ApplicationInstance arg) {
		ApplicationInstance o = (ApplicationInstance) arg;
		int result = this.getApplication().getName().compareTo(this.getApplication().getName());
		if(result == 0){
			result = this.getCreateTimestamp().compareTo(o.getCreateTimestamp());
		}
		return result;
	}
	
}
