package pl.edu.agh.neolog4j.dao.query;

public enum LogEventField {
	DATE("Date");
	
	private String name;
	
	public String val(){
		return this.name;
	}
	
	private LogEventField(String name){
		this.name = name;
	}
}
