package pl.edu.agh.neolog4j.dao;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.conversion.EndResult;
import org.springframework.data.neo4j.support.Neo4jTemplate;
import org.springframework.stereotype.Service;

import pl.edu.agh.neolog4j.model.AbstractModelClass;

@Service
public abstract class AbstractDao {
	
	@Autowired
	protected Neo4jTemplate template;	
	
	@SuppressWarnings("deprecation")
	protected <T> T getOrCreateNamedNode(String p_name, String p_indexName, Class<T> p_class) {
		HashMap<String, Object> prop = new HashMap<String, Object>();
		prop.put(p_indexName, p_name);
		T result = null;
		Transaction tx = this.template.getGraphDatabase().beginTx();
		try {	
			Node node = this.template.getOrCreateNode(p_indexName, p_indexName, p_name, prop);
			this.template.postEntityCreation(node, p_class);
			result = this.template.createEntityFromState(node, p_class, null);
			tx.success();
		} finally {
			tx.finish();
		}
		return result;
	}
	
	public <T> T requery(T p_source, Class<T> p_class) {
		return this.template.findOne(((AbstractModelClass)p_source).getGraphID(), p_class);
	}
	
	protected <T> T createNewNode(Class<T> p_class, Map<String, Object> p_properties) {
		Transaction tx = this.template.getGraphDatabase().beginTx();
		try {	
			T newNode = this.template.createNodeAs(p_class, p_properties);
			tx.success();
			return newNode;
		} finally {
			tx.finish();
		}
	}
	
	public <T> Set<T> getAllNodes(Class<T> p_class) {
		Set<T> result = new HashSet<T>();
		EndResult<T> findAll = this.template.repositoryFor(p_class).findAll();
		Iterator<T> iterator = findAll.iterator();
		while (iterator.hasNext()) {
			result.add(iterator.next());
		}
		return result;
	}
	
	public Neo4jTemplate getTemplate() {
		
		return this.template;
	}
	
	protected <E> Set<E> iterableToSet(Iterable<E> p_iterable) {
		
		Set<E> set = new LinkedHashSet<E>();
		for (E item: p_iterable) {
			set.add(item);
		}
		return set;
	}
}
