package pl.edu.agh.neolog4j.model;

public interface UniqueNamedNodesInterface {

	public String getName();
	public void setName(String p_name);
}
