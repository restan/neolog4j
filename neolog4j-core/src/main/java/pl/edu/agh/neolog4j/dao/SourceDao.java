package pl.edu.agh.neolog4j.dao;

import org.neo4j.graphdb.Transaction;
import org.springframework.stereotype.Service;

import pl.edu.agh.neolog4j.model.SourceClass;
import pl.edu.agh.neolog4j.model.SourcePackage;

@Service
public class SourceDao extends AbstractDao {
	
	public SourceClass getSource(String p_path) {
		SourceClass sourceClass = null;
		Transaction tx = this.template.getGraphDatabase().beginTx();
		try {			
			sourceClass = getOrCreateNamedNode(p_path, SourceClass.SOURCE_CLASS_INDEX, SourceClass.class);
			int dotIndex = p_path.lastIndexOf('.');
			if (sourceClass.getParentPackage() == null && dotIndex > 0) {
				SourcePackage sourcePackage = getSourcePackage(p_path.substring(0, p_path.lastIndexOf('.')));
				sourceClass.setParentPackage(sourcePackage);
			}
			this.template.save(sourceClass);
			tx.success();
		} finally {
			tx.finish();
		}
		return sourceClass;
	}
	
	public SourcePackage getSourcePackage(String p_path) {
		SourcePackage sourcePackage = null;
		Transaction tx = this.template.getGraphDatabase().beginTx();
		try {			
			sourcePackage = getOrCreateNamedNode(p_path, SourcePackage.SOURCE_PACKAGE_INDEX, SourcePackage.class);
			int dotIndex = p_path.lastIndexOf('.');
			if (sourcePackage.getParentPackage() == null && dotIndex > 0) {
				SourcePackage parentPackage = getSourcePackage(p_path.substring(0, p_path.lastIndexOf('.')));
				sourcePackage.setParentPackage(parentPackage);
			}
			this.template.save(sourcePackage);
			tx.success();
		} finally {
			tx.finish();
		}
		return sourcePackage;
	}
	
}
