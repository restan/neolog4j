package pl.edu.agh.neolog4j.dao.repository;

import java.util.Set;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;

import pl.edu.agh.neolog4j.model.Application;
import pl.edu.agh.neolog4j.model.ApplicationInstance;
import pl.edu.agh.neolog4j.model.Endpoint;
import pl.edu.agh.neolog4j.model.RelationNames;

public interface ApplicationRepository extends GraphRepository<Application> {

	final static String APPLICATION_INSTANCES_QUERY_START = "start app = node({0})" +
			"match app <-[:" + RelationNames.APPLICATION_INSTANCE_APPLICATION + "]-(instance) ";
	final static String RETURN_INSTANCE_STRING = "return instance";
	final static String APPLICATION_INSTANCES_QUERY_STRING = APPLICATION_INSTANCES_QUERY_START + RETURN_INSTANCE_STRING;
	
	final static String FROM_PARAMETER_NAME = "fromDate";
	final static String TIMESTAMP_PARAMETER_NAME = "timestamp";
	final static String INSTANCES_FROM_QUERY_STRING = APPLICATION_INSTANCES_QUERY_START
			+ "WHERE instance.createDateTimestamp >= {"
			+ FROM_PARAMETER_NAME
			+ "} " + RETURN_INSTANCE_STRING;
	
	final static String INSTANCES_BY_TIMESTAMPS_QUERY_STRING = APPLICATION_INSTANCES_QUERY_START
			+ "where instance.createDateTimestamp IN {"
			+ TIMESTAMP_PARAMETER_NAME + "} " + RETURN_INSTANCE_STRING;
	
	final static String APPLICAION_ENDPOINTS_QUERY_STRING = "start app = node({0})"
			+ "match app <-[:"
			+ RelationNames.ENDPOINT_APPLICATION
			+ "]-(endpoint) return endpoint";
	
	@Query(value = APPLICATION_INSTANCES_QUERY_STRING)
	public Iterable<ApplicationInstance> getInstances(Application p_application);
	
	@Query(value = APPLICAION_ENDPOINTS_QUERY_STRING)
	public Iterable<Endpoint> getEndpoints(Application p_application);
	
	@Query(value = INSTANCES_FROM_QUERY_STRING)
	public Iterable<ApplicationInstance> getInstances(
			@Param(value = "0") Application p_application,
			@Param(value = FROM_PARAMETER_NAME) long p_fromTimestamp);
	
	@Query(value = INSTANCES_BY_TIMESTAMPS_QUERY_STRING)
	public Iterable<ApplicationInstance> getInstances(
			@Param(value = "0") Application p_application,
			@Param(value = TIMESTAMP_PARAMETER_NAME) Set<Long> p_timestamps);
}
