package pl.edu.agh.neolog4j.model;

import java.util.Comparator;

import pl.edu.agh.neolog4j.dao.query.LogEventField;

public class NeoLogEventComparator implements Comparator<NeoLogEvent>{

	
	protected LogEventField compareField = null;
	
	public NeoLogEventComparator(LogEventField p_field) {
		this.compareField = p_field;
	}
	
	public int compare(NeoLogEvent p_event1, NeoLogEvent p_event2) {
		
		switch (this.compareField) {
		case DATE:
			return (int) (p_event1.getTimestamp() - p_event2.getTimestamp());
		default:
			break;
		}
		return 0;
	}

}
