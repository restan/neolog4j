package pl.edu.agh.neolog4j.utils.json;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

public class JsonDateSerializer extends JsonSerializer<Date> {
	 
    private static final SimpleDateFormat DATE_FORMAT =
    		new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
 
    @Override
	public final void serialize(
			final Date date,
			final JsonGenerator gen,
			final SerializerProvider provider)
					throws IOException {
 
        String formattedDate = DATE_FORMAT.format(date);
 
        gen.writeString(formattedDate);
    }
 
}
