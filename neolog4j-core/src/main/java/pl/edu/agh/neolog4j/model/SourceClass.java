package pl.edu.agh.neolog4j.model;

import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

@NodeEntity
public class SourceClass extends AbstractModelClass implements UniqueNamedNodesInterface {
	
	public final static String SOURCE_CLASS_INDEX = "name";
	
	@Indexed(indexName = SOURCE_CLASS_INDEX, unique = true) 
	private String name;
	
	@RelatedTo(type = RelationNames.SOURCE_CLASS_PACKAGE)
	private SourcePackage parentPackage;
	
	private SourceClass() {
		
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public SourcePackage getParentPackage() {
		return parentPackage;
	}
	public void setParentPackage(SourcePackage parentPackage) {
		this.parentPackage = parentPackage;
	}
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("SourceClass [");
		sb.append("name=").append(this.name).append(", ");
		sb.append("parentPackage=").append(this.parentPackage != null ? this.parentPackage.toString() : null).append(", ");
		sb.append("]");
		return sb.toString();
	}
	
}
