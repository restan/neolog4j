package pl.edu.agh.neolog4j.model;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

import pl.edu.agh.neolog4j.utils.json.JsonDateSerializer;

@NodeEntity
public class NeoLogEvent extends AbstractModelClass {
	private String level;
	private String loggerName;
	private String message;
	private String threadName;
	private long timestamp;
	
	@RelatedTo(type = RelationNames.LOG_EVENT_SOURCE)
	@JsonIgnore
	private SourceClass source;
	
	@RelatedTo(type = RelationNames.LOG_EVENT_APPLICATION_INSTANCE)
	@JsonIgnore
	private ApplicationInstance applicationInstance;
	
	@RelatedTo(type = RelationNames.LOG_EVENT_PREVIOUS)
	@JsonIgnore
	private NeoLogEvent previous;
	
	@RelatedTo(type = RelationNames.LOG_EVENT_SOURCE_ENDPOINT)
	@JsonIgnore
	@Fetch
	private InEndpoint sourceEndpoint;
	
	@RelatedTo(type = RelationNames.LOG_EVENT_DESTINATION_ENDPOINT)
	@JsonIgnore
	@Fetch
	private OutEndpoint destinationEndpoint;
	
	public NeoLogEvent() {
		
	}
	
	public String getLevel() {
		return level;
	}
	
	public void setLevel(String level) {
		this.level = level;
	}
	
	public String getLoggerName() {
		return loggerName;
	}
	
	public void setLoggerName(String loggerName) {
		this.loggerName = loggerName;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getThreadName() {
		return threadName;
	}
	
	public void setThreadName(String threadName) {
		this.threadName = threadName;
	}
	
	public long getTimestamp() {
		return timestamp;
	}
	
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	@JsonSerialize(using=JsonDateSerializer.class)
	public Date getDatetime(){
		return new Date(this.timestamp);
	}
	
	public SourceClass getSource() {
		return source;
	}
	
	public void setSource(SourceClass source) {
		this.source = source;
	}

	public NeoLogEvent getPrevious() {
		return previous;
	}

	public void setPrevious(NeoLogEvent previous) {
		this.previous = previous;
	}

	public ApplicationInstance getApplicationInstance() {
		return applicationInstance;
	}

	public void setApplicationInstance(ApplicationInstance applicationInstance) {
		this.applicationInstance = applicationInstance;
	}

	public InEndpoint getSourceEndpoint() {
		return sourceEndpoint;
	}

	public void setSourceEndpoint(InEndpoint sourceEndpoint) {
		this.sourceEndpoint = sourceEndpoint;
	}

	public OutEndpoint getDestinationEndpoint() {
		return destinationEndpoint;
	}

	public void setDestinationEndpoint(OutEndpoint destinationEndpoint) {
		this.destinationEndpoint = destinationEndpoint;
	}
	
	
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append("NeoLogEvent [");
		sb.append("level=").append(this.level).append(", ");
		sb.append("loggerName=").append(this.loggerName).append(", ");
		sb.append("message=").append(this.message).append(", ");
		sb.append("threadName=").append(this.threadName).append(", ");
		sb.append("timestamp=").append(this.timestamp).append(", ");
		sb.append("applicationInstance=").append(this.applicationInstance != null ? this.applicationInstance.toString() : null).append(", ");
		sb.append("]");
		return sb.toString();
	}
}
