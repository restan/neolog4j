package pl.edu.agh.neolog4j.model;

import org.springframework.data.neo4j.annotation.GraphId;

public class AbstractModelClass {

	@GraphId
	private Long graphID = null;
	
	@Override
	public boolean equals(Object obj) {
		
		if (this == obj) {
			return true;
		}
		if (obj == null || !getClass().equals(obj.getClass())) {			
			return false;
		}
		AbstractModelClass other = (AbstractModelClass) obj;
		if (this.getGraphID() == null) {			
			return super.equals(other);
		}
		return this.getGraphID().equals(other.getGraphID());
	}
	
	@Override
	public int hashCode() {
		
		return (this.getGraphID() == null) ? super.hashCode() : this.getGraphID().hashCode();
	}

	public Long getGraphID() {
		return this.graphID;
	}
	
}
