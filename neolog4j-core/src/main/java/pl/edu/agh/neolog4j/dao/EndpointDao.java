package pl.edu.agh.neolog4j.dao;

import org.neo4j.graphdb.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.edu.agh.neolog4j.model.Application;
import pl.edu.agh.neolog4j.model.Endpoint;
import pl.edu.agh.neolog4j.model.InEndpoint;
import pl.edu.agh.neolog4j.model.OutEndpoint;

@Service
public class EndpointDao extends AbstractDao {
	
	@Autowired protected ApplicationDao applicationDao;
	
	public OutEndpoint getOutEndpoint(String name, String appName){
		
		OutEndpoint outEndpoint = null;
		Transaction tx = this.template.getGraphDatabase().beginTx();
		try {			
			outEndpoint = getOrCreateNamedNode(
					//Endpoint.getFullEndpointName(name, appName),
					name,
					Endpoint.ENDPOINT_INDEX, OutEndpoint.class);
			if (outEndpoint.getApplication() == null) {
				Application application = this.applicationDao.getApplication(appName);
				outEndpoint.setApplication(application);
				this.template.save(outEndpoint);
			}
			tx.success();
		} finally {
			tx.finish();
		}
		return outEndpoint;
	}

	public InEndpoint getInEndpoint(String name, String appName) {
		InEndpoint inEndpoint = null;
		Transaction tx = this.template.getGraphDatabase().beginTx();
		try {			
			inEndpoint = getOrCreateNamedNode(
					//Endpoint.getFullEndpointName(name, appName),
					name,
					Endpoint.ENDPOINT_INDEX, InEndpoint.class);
			if (inEndpoint.getApplication() == null) {
				Application application = this.applicationDao.getApplication(appName);
				inEndpoint.setApplication(application);
				this.template.save(inEndpoint);
			}
			tx.success();
		} finally {
			tx.finish();
		}
		return inEndpoint;
	}
	
	public void connect(InEndpoint p_in, OutEndpoint p_out) {
		if (p_in != null && p_out != null) {
			Transaction tx = this.template.getGraphDatabase().beginTx();
			try {
				InEndpoint requeryIn = requery(p_in, InEndpoint.class);
				requeryIn.getSources().add(p_out);
				OutEndpoint requeryOut = requery(p_out, OutEndpoint.class);
				requeryOut.getDestinations().add(p_in);
				p_in.setSources(requeryIn.getSources());
				p_out.setDestinations(requeryOut.getDestinations());
				tx.success();
			} finally {
				tx.finish();
			}
		}
	}
	
	public void disconnectEndpoints(InEndpoint p_in, OutEndpoint p_out) {
		if (p_in != null && p_out != null) {
			Transaction tx = this.template.getGraphDatabase().beginTx();
			try {
				InEndpoint requeryIn = requery(p_in, InEndpoint.class);
				requeryIn.getSources().remove(p_out);
				OutEndpoint requeryOut = requery(p_out, OutEndpoint.class);
				requeryOut.getDestinations().remove(p_in);
				p_in.setSources(requeryIn.getSources());
				p_out.setDestinations(requeryOut.getDestinations());
				tx.success();
			} finally {
				tx.finish();
			}
		}
	}
}
