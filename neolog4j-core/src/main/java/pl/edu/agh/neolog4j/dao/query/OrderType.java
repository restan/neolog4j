package pl.edu.agh.neolog4j.dao.query;

public enum OrderType {
	ASC,
	DESC
}
