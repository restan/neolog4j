package pl.edu.agh.neolog4j.dao.repository;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

import pl.edu.agh.neolog4j.model.NeoLogEvent;
import pl.edu.agh.neolog4j.model.RelationNames;

public interface NeoLogEventRepository extends GraphRepository<NeoLogEvent> {

	final static String NEXT_EVENTS_QUERY_STRING = "start n=node({0})" +
			"match n<-[:" + RelationNames.LOG_EVENT_PREVIOUS + "]-(next)" +
					"return next";
	
	@Query(value = NEXT_EVENTS_QUERY_STRING)
	public Iterable<NeoLogEvent> getNext(NeoLogEvent p_logEvent);
}
