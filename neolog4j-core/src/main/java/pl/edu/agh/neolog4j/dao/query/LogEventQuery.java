package pl.edu.agh.neolog4j.dao.query;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import pl.edu.agh.neolog4j.model.AbstractModelClass;
import pl.edu.agh.neolog4j.model.ApplicationInstance;
import pl.edu.agh.neolog4j.model.RelationNames;

public class LogEventQuery {
	
	
	private Set<LogLevel> levels = new HashSet<LogLevel>();
	private Set<ApplicationInstance> applicationInstances = new HashSet<ApplicationInstance>();
	private Set<String> inEndpoits = new HashSet<String>();
	private Set<String> outEndpoits = new HashSet<String>();
	private Date after;
	private Date before;
	private LogEventField orderBy = LogEventField.DATE;
	private OrderType orderType = OrderType.ASC;
	private int offset = 0;
	private int limit = 0;
	
	public void setAfter(Date after) {
		this.after = after;
	}

	public void setBefore(Date before) {
		this.before = before;
	}

	public void setOrderBy(LogEventField orderBy) {
		this.orderBy = orderBy;
	}

	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}
	
	public LogEventQuery before(Date before){
		this.before = before;
		return this;
	}
	
	public LogEventQuery after(Date after){
		this.after = after;
		return this;
	}
	
	public LogEventQuery orderBy(LogEventField field){
		this.orderBy = field;
		return this;
	}
	
	public LogEventQuery orderBy(LogEventField field, OrderType orderType){
		this.orderBy = field;
		this.orderType = orderType;
		return this;
	}
	
	public LogEventQuery offset(int offset){
		this.offset= offset;
		return this;
	}
	
	public LogEventQuery limit(int limit){
		this.limit = limit;
		return this;
	}
	
	public LogEventQuery setLevels(Set<LogLevel> levels){
		this.levels = levels;
		return this;
	}
	
	public LogEventQuery addLevel(LogLevel level){
		this.levels.add(level);
		return this;
	}
	
	public LogEventQuery setApplicationInstances(Set<ApplicationInstance> applicationInstances){
		this.applicationInstances = applicationInstances;
		return this;
	}
	
	public LogEventQuery addApplicationInstance(ApplicationInstance applicationInstance){
		this.applicationInstances.add(applicationInstance);
		return this;
	}
	
	public LogEventQuery addApplicationInstances(Set<ApplicationInstance> applicationInstances){
		this.applicationInstances.addAll(applicationInstances);
		return this;
	}
	
	public void addInEndpoint(String p_inEndpointName) {
		this.inEndpoits.add(p_inEndpointName);
	}
	
	public void setInEndpoints(Set<String> p_endpoints) {
		this.inEndpoits = p_endpoints;
	}
	
	public void addOutEndpoint(String p_outEndpointName) {
		this.outEndpoits.add(p_outEndpointName);
	}
	
	public void setOutEndpoints(Set<String> p_endpoints) {
		this.outEndpoits = p_endpoints;
	}
	
	// getters
	
	public Set<String> getInEndpoints() {
		return this.inEndpoits;
	}
	
	public Set<String> getOutEndpoints() {
		return this.outEndpoits;
	}
	
	public Date getAfter() {
		return after;
	}

	public Date getBefore() {
		return before;
	}

	public LogEventField getOrderBy() {
		return orderBy;
	}

	public OrderType getOrderType() {
		return orderType;
	}

	public int getOffset() {
		return offset;
	}

	public int getLimit() {
		return limit;
	}

	public Set<LogLevel> getLevels() {
		return levels;
	}

	public Set<ApplicationInstance> getApplicationInstances() {
		return applicationInstances;
	}
	
	protected Set<String> getLevelsAsStrings() {
		
		Set<String> result = new HashSet<String>();
		for (LogLevel level: this.levels) {
			result.add("'" + level.val() + "'");
		}
		return result;
	}
		
	public String getCypherQueryString() {
		StringBuilder builder = new StringBuilder();
		StringBuilder startString = getStartString();
		if (startString.length() > 0) {
			builder.append(startString);
			builder.append(getDateQueryString());
			builder.append(getLevelQueryString());
			builder.append(getEndpointsQueryString());
			builder.append(" return log");
			builder.append(getOrderByString());
			if (this.limit > 0) {
				builder.append(" limit " + this.limit);
			}
		}
		return builder.toString();
	}
	
	protected StringBuilder getStartString() {
		StringBuilder builder = new StringBuilder();
		builder.append("start ins = node(");
		if (!this.applicationInstances.isEmpty()) {
			builder.append(getIdsString(this.applicationInstances));
			builder.append(" match ins <-[:"
					+ RelationNames.LOG_EVENT_APPLICATION_INSTANCE + "]- (log)");
			if (!this.inEndpoits.isEmpty() || !this.outEndpoits.isEmpty()) {
				builder.append("-[:" + RelationNames.LOG_EVENT_SOURCE_ENDPOINT
						+ "|" + RelationNames.LOG_EVENT_DESTINATION_ENDPOINT
						+ "]->(endp)");
			}
		} 
		return builder;
	}
	
	
	
	
	
	protected StringBuilder getIdsString(Set<? extends AbstractModelClass> p_set) {
		StringBuilder builder = new StringBuilder();
		String prefix = "";
		for (AbstractModelClass instance : p_set) {
			builder.append(prefix);
			prefix = ", ";
			builder.append(instance.getGraphID());
		}
		builder.append(")");
		return builder;
	}
	
	protected StringBuilder getDateQueryString() {
		StringBuilder builder = new StringBuilder();
		if (this.after != null) {			
			builder.append("where log.timestamp > " + after.getTime());
		}
		if (this.before != null) {
			if (this.after != null) {
				builder.append(" and");
			} else {
				builder.append("where");
			}
			builder.append(" log.timestamp < " + before.getTime());
		}
		return builder;
	}
	
	protected StringBuilder getLevelQueryString() {
		StringBuilder builder = new StringBuilder();
		if (this.levels != null && !this.levels.isEmpty()) {			
			if (this.after != null || this.before != null) {
				builder.append(" and");
			}else {
				builder.append("where");
			}
			builder.append(" log.level IN " + getLevelsAsStrings());
		}
		return builder;
	}
	
	protected StringBuilder getEndpointsQueryString() {
		StringBuilder builder = new StringBuilder();
		if (!this.inEndpoits.isEmpty() || !this.outEndpoits.isEmpty()) {			
			if (!this.levels.isEmpty() || this.after != null || this.before != null) {
				builder.append(" and");
			} else {
				builder.append(" where");
			}
			builder.append(" endp.name IN " + getEndpointsNames());
		}
		return builder;
	}
	
	protected Set<String> getEndpointsNames() {
		Set<String> result = new HashSet<String>();
		for (String endpoint: this.inEndpoits) {
			result.add("'" + endpoint + "'");
		}
		for (String endpoint: this.outEndpoits) {
			result.add("'" + endpoint + "'");
		}
		return result;
	}
	
	protected StringBuilder getOrderByString() {
		StringBuilder builder = new StringBuilder();
		if (this.orderBy != null) {
			builder.append(" order by log." + getOrderByFieldNameString());
			if (this.orderType == OrderType.DESC) {
				builder.append(" desc");
			}
		}
		return builder;
	}
	
	public String getOrderByFieldNameString() {
		switch (this.orderBy) {
		case DATE:
			return "timestamp";
		default:
			break;
		}
		return null;
	}
	
}
