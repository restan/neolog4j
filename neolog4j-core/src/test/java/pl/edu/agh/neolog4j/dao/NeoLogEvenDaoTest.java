package pl.edu.agh.neolog4j.dao;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import pl.edu.agh.neolog4j.AbstractTest;
import pl.edu.agh.neolog4j.dao.query.LogEventQuery;
import pl.edu.agh.neolog4j.dao.query.LogLevel;
import pl.edu.agh.neolog4j.dao.query.OrderType;
import pl.edu.agh.neolog4j.model.ApplicationInstance;
import pl.edu.agh.neolog4j.model.InEndpoint;
import pl.edu.agh.neolog4j.model.NeoLogEvent;
import pl.edu.agh.neolog4j.model.OutEndpoint;
import pl.edu.agh.neolog4j.model.SimpleLogEvent;
import pl.edu.agh.neolog4j.model.SourceClass;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:META-INF/spring/test-applicationContext.xml")
public class NeoLogEvenDaoTest extends AbstractTest {

	protected final static String TEST_APP_NAME = "testApp";
	protected final static String TEST_MSG = "testMsg";
	protected final static String TEST_LEVEL_ERROR = "ERROR";
	protected final static String TEST_LEVEL_DEBUG = "DEBUG";
	protected final static String TEST_LOGGER_NAME = "loggerName";
	protected final static String TEST_THREAD_NAME = "threadName";
	protected final static long TEST_TIMESTAMP = 1L; 
	protected final static String TEST_DECLARING_CLASS = "TestClass";
	protected final static String TEST_DECLARING_METHOD = "testMethod";
	protected final static String TEST_DECLARING_FILE = "testFile";
	protected final static int TEST_LINE_NUMBER = 23;
	protected final static String TEST_IN_ENDPOINT = "testIn";
	protected final static String TEST_OUT_ENDPOINT = "testOut";
	
	@Autowired 
	protected NeoLogEventDao dao;
	
	@Autowired 
	protected ApplicationDao applicationDao;
	
	@Autowired
	protected SourceDao sourceDao;
	
	@Autowired
	protected EndpointDao endpointDao;
	
	@Test
	public void testSimpleEventGet() {
		
		NeoLogEvent neoLogEvent = this.dao.getNeoLogEvent();
		NeoLogEvent neoLogEvent2 = this.dao.getNeoLogEvent();
		assertNotNull(neoLogEvent.getGraphID());
		assertNotNull(neoLogEvent2.getGraphID());
		assertNotEquals(neoLogEvent, neoLogEvent2);
	}
	
	@Test
	public void testEventGet() {
		ApplicationInstance applicationInstance = this.applicationDao.getApplicationInstance(TEST_APP_NAME);
		NeoLogEvent neoLogEvent = this.dao.getNeoLogEvent(getSimpleLogEvent(), applicationInstance);
		neoLogEvent = this.dao.requery(neoLogEvent, NeoLogEvent.class);
		assertTrue(equal(getSimpleLogEvent(), neoLogEvent));
		assertEquals(neoLogEvent.getApplicationInstance(), applicationInstance);
		NeoLogEvent neoLogEvent2 = this.dao.getNeoLogEvent(getSimpleLogEvent(), applicationInstance);
		assertNotEquals(neoLogEvent, neoLogEvent2);
	}
	
	@Test
	public void testEventWithPrevGet() {
		NeoLogEvent neoLogEvent = this.dao.getNeoLogEvent();
		NeoLogEvent neoLogEvent2 = this.dao.getNeoLogEvent(null, neoLogEvent, null);
		neoLogEvent2 = this.dao.requery(neoLogEvent2, NeoLogEvent.class);
		assertNotEquals(neoLogEvent, neoLogEvent2);
		assertEquals(neoLogEvent, neoLogEvent2.getPrevious());
	}
	
	@Test
	public void testSimpleQuery() {
		ApplicationInstance applicationInstance = this.applicationDao.getApplicationInstance(TEST_APP_NAME);
		ApplicationInstance applicationInstance2 = this.applicationDao.getApplicationInstance(TEST_APP_NAME);
		SimpleLogEvent simpleLogEvent = getSimpleLogEvent();
		Set<NeoLogEvent> instanceLogs = new HashSet<NeoLogEvent>();
		Set<NeoLogEvent> instanceLogs2 = new HashSet<NeoLogEvent>();
		for (int i = 0; i < 5; i++) {
			instanceLogs.add(this.dao.getNeoLogEvent(simpleLogEvent, applicationInstance));
		}
		for (int i = 0; i < 10; i++) {
			instanceLogs2.add(this.dao.getNeoLogEvent(simpleLogEvent, applicationInstance2));
		}
		LogEventQuery query = new LogEventQuery();
		query.addApplicationInstance(applicationInstance);
		Set<NeoLogEvent> queriedLogs = this.dao.query(query);
		assertEquals(instanceLogs, queriedLogs);
		query.addApplicationInstance(applicationInstance2);
		queriedLogs = this.dao.query(query);
		instanceLogs.addAll(instanceLogs2);
		assertEquals(instanceLogs, queriedLogs);
	}
	
	@Test
	public void testQueryWithLimit() {
		ApplicationInstance applicationInstance = this.applicationDao.getApplicationInstance(TEST_APP_NAME);
		SimpleLogEvent simpleLogEvent = getSimpleLogEvent();
		for (int i = 0; i < 10; i++) {
			this.dao.getNeoLogEvent(simpleLogEvent, applicationInstance);
		}
		LogEventQuery query = new LogEventQuery();
		query.addApplicationInstance(applicationInstance);
		Set<NeoLogEvent> queriedLogs = this.dao.query(query);
		assertEquals(10, queriedLogs.size());
		query.setLimit(3);
		queriedLogs = this.dao.query(query);
		assertEquals(3, queriedLogs.size());
	}
	
	@Test
	public void testQueryWithOrderBy() {
		ApplicationInstance applicationInstance = this.applicationDao.getApplicationInstance(TEST_APP_NAME);
		SimpleLogEvent simpleLogEvent = getSimpleLogEvent();
		List<NeoLogEvent> logsList = new ArrayList<NeoLogEvent>();
		for (int i = 0; i < 10; i++) {
			simpleLogEvent.setTimestamp(i + 1);
			logsList.add(this.dao.getNeoLogEvent(simpleLogEvent, applicationInstance));
		}
		List<NeoLogEvent> reversedList = new ArrayList<NeoLogEvent>(logsList);
		Collections.reverse(reversedList);
		LogEventQuery query = new LogEventQuery();
		query.addApplicationInstance(applicationInstance);
		List<NeoLogEvent> queriedLogs = new ArrayList<NeoLogEvent>(dao.query(query));
		assertEquals(logsList, queriedLogs);
		assertNotEquals(reversedList, queriedLogs);
		query.setOrderType(OrderType.DESC);
		queriedLogs = new ArrayList<NeoLogEvent>(dao.query(query));
		assertNotEquals(logsList, queriedLogs);
		assertEquals(reversedList, queriedLogs);
	}
	
	@Test
	public void testQueryWithDates() {
		int year = 1989;
		int month = 4;
		int day = 5;
		ApplicationInstance applicationInstance = this.applicationDao.getApplicationInstance(TEST_APP_NAME);
		SimpleLogEvent simpleLogEvent = getSimpleLogEvent();
		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month, day);
		for (int i = 0; i < 10; i++) {
			simpleLogEvent.setTimestamp(calendar.getTimeInMillis());
			this.dao.getNeoLogEvent(simpleLogEvent, applicationInstance);
			incrementDay(calendar);
		}
		LogEventQuery query = new LogEventQuery();
		query.addApplicationInstance(applicationInstance);
		calendar.set(Calendar.DAY_OF_MONTH, day);
		query.setBefore(calendar.getTime());
		Set<NeoLogEvent> queriedLogs = this.dao.query(query);
		assertEquals(0, queriedLogs.size());
		query.setBefore(null);
		calendar.set(Calendar.DAY_OF_MONTH, day + 9);
		query.setAfter(calendar.getTime());
		queriedLogs = this.dao.query(query);
		assertEquals(0, queriedLogs.size());
		calendar.set(Calendar.DAY_OF_MONTH, day + 8);
		Date before = calendar.getTime();
		query.setBefore(before);
		query.setAfter(null);
		queriedLogs = this.dao.query(query);
		assertEquals(8, queriedLogs.size());
		query.setBefore(null);
		calendar.set(Calendar.DAY_OF_MONTH, day + 5);
		Date after = calendar.getTime();
		query.setAfter(after);
		queriedLogs = this.dao.query(query);
		assertEquals(4, queriedLogs.size());
		query.setBefore(before);
		queriedLogs = this.dao.query(query);
		assertEquals(2, queriedLogs.size());
		calendar.set(Calendar.DAY_OF_MONTH, day);
		query.setBefore(calendar.getTime());
		queriedLogs = this.dao.query(query);
		assertEquals(0, queriedLogs.size());
		query.setBefore(after);
		query.setAfter(before);
		queriedLogs = this.dao.query(query);
		assertEquals(0, queriedLogs.size());
	}
	
	protected void incrementDay(Calendar p_calendar) {
		int currentDay = p_calendar.get(Calendar.DAY_OF_MONTH);
		p_calendar.set(Calendar.DAY_OF_MONTH, currentDay + 1);
	}
	
	@Test
	public void testQueryWithLevels() {
		ApplicationInstance applicationInstance = this.applicationDao.getApplicationInstance(TEST_APP_NAME);
		SimpleLogEvent simpleLogEvent = getSimpleLogEvent();
		Set<NeoLogEvent> debugLogs = new HashSet<NeoLogEvent>();
		Set<NeoLogEvent> errorLogs = new HashSet<NeoLogEvent>();
		simpleLogEvent.setLevel(TEST_LEVEL_ERROR);
		for (int i = 0; i < 5; i++) {
			errorLogs.add(this.dao.getNeoLogEvent(simpleLogEvent, applicationInstance));
		}
		simpleLogEvent.setLevel(TEST_LEVEL_DEBUG);
		for (int i = 0; i < 5; i++) {
			debugLogs.add(this.dao.getNeoLogEvent(simpleLogEvent, applicationInstance));
		}
		LogEventQuery query = new LogEventQuery();
		query.addLevel(LogLevel.ERROR);
		query.addApplicationInstance(applicationInstance);
		Set<NeoLogEvent> queriedLogs = this.dao.query(query);
		assertEquals(errorLogs, queriedLogs);
		assertNotEquals(debugLogs, queriedLogs);
		query.addLevel(LogLevel.DEBUG);
		queriedLogs = this.dao.query(query);
		Set<NeoLogEvent> allLogs = new HashSet<NeoLogEvent>(debugLogs);
		allLogs.addAll(errorLogs);
		assertEquals(allLogs, queriedLogs);
		assertNotEquals(errorLogs, queriedLogs);
		assertNotEquals(debugLogs, queriedLogs);
	}
	
	@Test
	public void testFullQuery() {
		ApplicationInstance applicationInstance = this.applicationDao.getApplicationInstance(TEST_APP_NAME);
		ApplicationInstance applicationInstance2 = this.applicationDao.getApplicationInstance(TEST_APP_NAME + "_2");
		OutEndpoint outEndpoint = this.endpointDao.getOutEndpoint(TEST_OUT_ENDPOINT, TEST_APP_NAME);
		InEndpoint inEndpoint = this.endpointDao.getInEndpoint(TEST_IN_ENDPOINT, TEST_APP_NAME);
		SimpleLogEvent simpleLogEvent = getSimpleLogEvent();
		List<NeoLogEvent> debugLogs = new ArrayList<NeoLogEvent>();
		simpleLogEvent.setLevel(TEST_LEVEL_ERROR);
		for (int i = 0; i < 5; i++) {
			simpleLogEvent.setTimestamp(new Date().getTime());
			NeoLogEvent neoLogEvent = this.dao.getNeoLogEvent(simpleLogEvent, applicationInstance);
			neoLogEvent.setSourceEndpoint(inEndpoint);
			this.dao.getTemplate().save(neoLogEvent);
			simpleLogEvent.setTimestamp(new Date().getTime());
			neoLogEvent = this.dao.getNeoLogEvent(simpleLogEvent, applicationInstance2);
			this.dao.getTemplate().save(neoLogEvent);
			neoLogEvent.setSourceEndpoint(inEndpoint);
		}
		simpleLogEvent.setLevel(TEST_LEVEL_DEBUG);
		for (int i = 0; i < 5; i++) {
			simpleLogEvent.setTimestamp(new Date().getTime());
			NeoLogEvent neoLogEvent = this.dao.getNeoLogEvent(simpleLogEvent, applicationInstance);
			neoLogEvent.setDestinationEndpoint(outEndpoint);
			this.dao.getTemplate().save(neoLogEvent);
			debugLogs.add(neoLogEvent);
			simpleLogEvent.setTimestamp(new Date().getTime());
			neoLogEvent = this.dao.getNeoLogEvent(simpleLogEvent, applicationInstance2);
			neoLogEvent.setDestinationEndpoint(outEndpoint);
			this.dao.getTemplate().save(neoLogEvent);
			debugLogs.add(neoLogEvent);
		}
		Collections.reverse(debugLogs);
		LogEventQuery query = new LogEventQuery();
		query.addLevel(LogLevel.DEBUG);
		query.setOrderType(OrderType.DESC);
		query.addApplicationInstance(applicationInstance);
		query.addApplicationInstance(applicationInstance2);
		query.setLimit(5);
		query.setBefore(new Date());
		Calendar calendar = Calendar.getInstance();
		calendar.set(1989, 4, 28);
		query.setAfter(calendar.getTime());
		query.addInEndpoint(inEndpoint.getName());
		List<NeoLogEvent> queriedLogs = new ArrayList<NeoLogEvent>(this.dao.query(query));
		assertEquals(0, queriedLogs.size());
		query.addOutEndpoint(outEndpoint.getName());
		queriedLogs = new ArrayList<NeoLogEvent>(this.dao.query(query));
		assertEquals(debugLogs.subList(0, 5), queriedLogs);	
	}
	
	@Test
	public void testQueryWithEndpoints() {
		ApplicationInstance applicationInstance = this.applicationDao.getApplicationInstance(TEST_APP_NAME);
		OutEndpoint outEndpoint = this.endpointDao.getOutEndpoint(TEST_OUT_ENDPOINT, TEST_APP_NAME);
		InEndpoint inEndpoint = this.endpointDao.getInEndpoint(TEST_IN_ENDPOINT, TEST_APP_NAME);
		SimpleLogEvent simpleLogEvent = getSimpleLogEvent();
		Set<NeoLogEvent> outLogs = new HashSet<NeoLogEvent>();
		Set<NeoLogEvent> inLogs = new HashSet<NeoLogEvent>();
		Set<NeoLogEvent> allLogs = new HashSet<NeoLogEvent>();
		for (int i = 0; i < 5; i++) {
			NeoLogEvent neoLogEvent = this.dao.getNeoLogEvent(simpleLogEvent, applicationInstance);
			neoLogEvent.setSourceEndpoint(inEndpoint);
			this.dao.getTemplate().save(neoLogEvent);
			inLogs.add(neoLogEvent);
			allLogs.add(neoLogEvent);
		}
		for (int i = 0; i < 5; i++) {
			NeoLogEvent neoLogEvent = this.dao.getNeoLogEvent(simpleLogEvent, applicationInstance);
			neoLogEvent.setDestinationEndpoint(outEndpoint);
			this.dao.getTemplate().save(neoLogEvent);
			outLogs.add(neoLogEvent);
			allLogs.add(neoLogEvent);
		}
		LogEventQuery query = new LogEventQuery();
		query.addApplicationInstance(applicationInstance);
		Set<NeoLogEvent> queriedLogs = this.dao.query(query);
		assertEquals(allLogs, queriedLogs);
		query.addInEndpoint(TEST_IN_ENDPOINT);
		queriedLogs = this.dao.query(query);
		assertEquals(inLogs, queriedLogs);
		query.setInEndpoints(new HashSet<String>());
		query.addOutEndpoint(TEST_OUT_ENDPOINT);
		queriedLogs = this.dao.query(query);
		assertEquals(outLogs, queriedLogs);
		query.addInEndpoint(TEST_IN_ENDPOINT);
		queriedLogs = this.dao.query(query);
		assertEquals(allLogs, queriedLogs);
	}
	
	@Test
	public void testGetNext() {
		NeoLogEvent neoLogEvent = this.dao.getNeoLogEvent();
		Set<NeoLogEvent> next = new HashSet<NeoLogEvent>();
		for (int i = 0; i < 10; i++) {
			NeoLogEvent nextEvent = this.dao.getNeoLogEvent();
			nextEvent.setPrevious(neoLogEvent);
			this.dao.getTemplate().save(nextEvent);
			next.add(nextEvent);
		}
		Set<NeoLogEvent> nextEvents = this.dao.getNextEvents(neoLogEvent);
		assertEquals(next, nextEvents);
	}
	
	protected SimpleLogEvent getSimpleLogEvent() {
		SimpleLogEvent result = new SimpleLogEvent();
		result.setLevel(TEST_LEVEL_ERROR);
		result.setLoggerName(TEST_LOGGER_NAME);
		result.setMessage(TEST_MSG);
		result.setThreadName(TEST_THREAD_NAME);
		result.setTimestamp(TEST_TIMESTAMP);
		StackTraceElement stackTraceElement = new StackTraceElement(
				TEST_DECLARING_CLASS, TEST_DECLARING_METHOD,
				TEST_DECLARING_FILE, TEST_LINE_NUMBER);
		result.setStackTraceElement(stackTraceElement);
		return result;
	}
	
	protected boolean equal(SimpleLogEvent p_simpleEvent, NeoLogEvent p_event) {
		if (!p_simpleEvent.getLevel().equals(p_event.getLevel())) {
			return false;
		}
		if (!p_simpleEvent.getLoggerName().equals(p_event.getLoggerName())) {
			return false;
		}
		if (!p_simpleEvent.getMessage().equals(p_event.getMessage())) {
			return false;
		}
		if (!p_simpleEvent.getThreadName().equals(p_event.getThreadName())) {
			return false;
		}
		if (!(p_simpleEvent.getTimestamp() == p_event.getTimestamp())) {
			return false;
		}
		StackTraceElement stackTraceElement = p_simpleEvent.getStackTraceElement();
		SourceClass source = this.sourceDao.getSource(stackTraceElement.getClassName());
		if (!(source.equals(p_event.getSource()))) {
			return false;
		}
		return true;
	}
}
