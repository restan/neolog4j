package pl.edu.agh.neolog4j.model;

import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Test;

import pl.edu.agh.neolog4j.dao.query.LogEventField;

public class NeoLogEventComparatorTest {
	
	@Test
	public void testCompareDate() {
		NeoLogEventComparator comparator = new NeoLogEventComparator(LogEventField.DATE);
		long timestamp = new Date().getTime();
		NeoLogEvent event1 = new NeoLogEvent();
		event1.setTimestamp(timestamp);
		NeoLogEvent event2 = new NeoLogEvent();
		event2.setTimestamp(timestamp + 100);
		NeoLogEvent event3 = new NeoLogEvent();
		event3.setTimestamp(timestamp);
		assertTrue(comparator.compare(event1, event2) < 0);
		assertTrue(comparator.compare(event2, event1) > 0);
		assertTrue(comparator.compare(event1, event3) == 0);
		assertTrue(comparator.compare(event3, event1) == 0);
	}

	@Test(expected = NullPointerException.class)
	public void testCompareNull() {
		NeoLogEventComparator comparator = new NeoLogEventComparator(null);
		NeoLogEvent event1 = new NeoLogEvent();
		NeoLogEvent event2 = new NeoLogEvent();
		comparator.compare(event1, event2);
	}
}
