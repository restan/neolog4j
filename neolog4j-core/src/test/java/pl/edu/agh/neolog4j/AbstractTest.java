package pl.edu.agh.neolog4j;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.neo4j.graphdb.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.support.Neo4jTemplate;

public class AbstractTest {

	protected static final String REMOVE_NODES = "START n = node(*) DELETE n";
	protected static final String REMOVE_RELS = "START r = rel(*) DELETE r";
	
	@Autowired
    private Neo4jTemplate template;

    @Before
    public void clearDb() {
    	Transaction tx = this.template.getGraphDatabase().beginTx();
    	try {    		
    		this.template.query(REMOVE_NODES, new HashMap<String, Object>());
    		this.template.query(REMOVE_RELS, new HashMap<String, Object>());
    		tx.success();
    	} finally {
    		tx.finish();
    	}
    }
}
