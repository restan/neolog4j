package pl.edu.agh.neolog4j.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import pl.edu.agh.neolog4j.model.Application;
import pl.edu.agh.neolog4j.model.ApplicationInstance;
import pl.edu.agh.neolog4j.model.Endpoint;
import pl.edu.agh.neolog4j.model.InEndpoint;
import pl.edu.agh.neolog4j.model.OutEndpoint;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:META-INF/spring/test-applicationContext.xml")
public class ApplicationDaoTest extends AbstractNamedEntitiesDaoTest<Application> {

	
	@Autowired protected ApplicationDao applicationDao;
	@Autowired protected EndpointDao endpointDao;
	
	protected final static String APP_NAME = "testAppName";
	protected final static int TEST_YEAR = 1989;
	protected final static int TEST_MONTH = 4;
	protected final static int TEST_DAY = 28;

	@Override
	protected Application getNode(String p_name) {
		return this.applicationDao.getApplication(p_name);
	}

	@Override
	protected Class<Application> getProcessedEntityClass() {
		return Application.class;
	}

	@Override
	protected AbstractDao getAbstractDao() {
		return this.applicationDao;
	}
	
	@Test
	public void getApplication() {
		Application application = this.applicationDao.getApplication(APP_NAME);
		assertNotNull(this.applicationDao.requery(application, Application.class).getGraphID());
		Application application2 = this.applicationDao.getApplication(APP_NAME);
		assertEquals(application, application2);
		Set<Application> allApplications = this.applicationDao.getAllNodes(Application.class);
		assertEquals(1, allApplications.size());
		Application application3 = this.applicationDao.getApplication(APP_NAME + "_2");
		assertNotEquals(application, application3);
		allApplications = this.applicationDao.getAllNodes(Application.class);
		assertEquals(2, allApplications.size());
	}
	
	@Test
	public void testGetApplicationInstance() {
		ApplicationInstance applicationInstance = this.applicationDao.getApplicationInstance(APP_NAME);
		ApplicationInstance req = this.applicationDao.requery(
				applicationInstance, ApplicationInstance.class);
		assertNotNull(req.getGraphID());
		ApplicationInstance requery = this.applicationDao.requery(
				applicationInstance, ApplicationInstance.class);
		assertEquals(applicationInstance, requery);
		assertEquals(APP_NAME, requery.getApplication().getName());
		ApplicationInstance applicationInstance2 = this.applicationDao.getApplicationInstance(APP_NAME);
		assertNotNull(applicationInstance2.getGraphID());
		assertEquals(APP_NAME, applicationInstance2.getApplication().getName());
		assertNotEquals(applicationInstance, applicationInstance2);
	}
	
	@Test
	public void testGetApplicationInstanceWithForcedDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(TEST_YEAR, TEST_MONTH, TEST_DAY);
		ApplicationInstance applicationInstance = this.applicationDao
				.getApplicationInstance(APP_NAME, calendar.getTime());
		ApplicationInstance requery = this.applicationDao.requery(applicationInstance, ApplicationInstance.class);
		assertEquals(applicationInstance, requery);
		assertEquals(new Long(calendar.getTimeInMillis()), requery.getCreateTimestamp());
	}
	
	@Test
	public void testGetApplications() {
		Set<Application> allApps = new HashSet<Application>();
		Application application = this.applicationDao.getApplication(APP_NAME);
		allApps.add(application);
		Set<Application> dbApps = this.applicationDao.getApplications();
		assertEquals(allApps, dbApps);
		Application application2 = this.applicationDao.getApplication(APP_NAME + "_2");
		allApps.add(application2);
		dbApps = this.applicationDao.getApplications();
		assertEquals(allApps, dbApps);
	}
	
	@Test(expected = DataRetrievalFailureException.class)
	public void testGetApplicationInstaceById() {
		ApplicationInstance applicationInstance = this.applicationDao
				.getApplicationInstance(APP_NAME);
		ApplicationInstance applicationInstance2 = this.applicationDao
				.getApplicationInstance(applicationInstance.getGraphID());
		assertEquals(applicationInstance, applicationInstance2);
		this.applicationDao.getApplicationInstance(9999);
	}
	
	@Test
	public void testGetApplicationInstances() {
		Application application = this.applicationDao.getApplication(APP_NAME);
		Set<ApplicationInstance> applicationInstances = new HashSet<ApplicationInstance>();
		this.applicationDao.getApplicationInstance(APP_NAME + "_2");
		Set<ApplicationInstance> dbInstances = this.applicationDao.getAllInstances(application);
		assertEquals(0, dbInstances.size());
		ApplicationInstance applicationInstance = this.applicationDao.getApplicationInstance(APP_NAME);
		applicationInstances.add(applicationInstance);
		applicationInstance = this.applicationDao.getApplicationInstance(APP_NAME);
		applicationInstances.add(applicationInstance);
		dbInstances = this.applicationDao.getAllInstances(application);
		assertEquals(applicationInstances, dbInstances);
	}
	
	@Test
	public void testGetEndpoints() {
		String endpointName1 = "OUT";
		String endpointName2 = "IN";
		Application application = this.applicationDao.getApplication(APP_NAME);
		Set<Endpoint> endpoints = new HashSet<Endpoint>();
		Set<Endpoint> dbInstances = this.applicationDao.getApplicationEndpoints(application);
		assertEquals(0, dbInstances.size());
		OutEndpoint outEndpoint = this.endpointDao.getOutEndpoint(endpointName1, APP_NAME);
		endpoints.add(outEndpoint);
		InEndpoint inEndpoint = this.endpointDao.getInEndpoint(endpointName2, APP_NAME);
		endpoints.add(inEndpoint);
		this.endpointDao.connect(inEndpoint, outEndpoint);
		dbInstances = this.applicationDao.getApplicationEndpoints(application);
		assertEquals(endpoints, dbInstances);
	}
	
	@Test
	public void testGetInstancesAfter() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(TEST_YEAR, TEST_MONTH, TEST_DAY);
		Calendar calendar2 = Calendar.getInstance();
		calendar2.set(TEST_YEAR, TEST_MONTH, TEST_DAY + 1);
		ApplicationInstance applicationInstance1 = this.applicationDao.getApplicationInstance(APP_NAME, calendar.getTime());
		ApplicationInstance applicationInstance2 = this.applicationDao.getApplicationInstance(APP_NAME);
		Set<ApplicationInstance> dbInstances = this.applicationDao.getInstances(APP_NAME, new Date());
		assertEquals(0, dbInstances.size());
		Set<ApplicationInstance> testSet = new HashSet<ApplicationInstance>();
		testSet.add(applicationInstance2);
		dbInstances = this.applicationDao.getInstances(APP_NAME, calendar2.getTime());
		assertEquals(testSet, dbInstances);
		testSet.add(applicationInstance1);
		dbInstances = this.applicationDao.getInstances(APP_NAME, calendar.getTime());
		assertEquals(testSet, dbInstances);
	}
	
	@Test
	public void testGetInstancesByTimestamps() {

		Calendar calendar = Calendar.getInstance();
		calendar.set(TEST_YEAR, TEST_MONTH, TEST_DAY);
		Calendar calendar2 = Calendar.getInstance();
		calendar2.set(TEST_YEAR, TEST_MONTH, TEST_DAY + 1);
		ApplicationInstance applicationInstance1 = this.applicationDao.getApplicationInstance(APP_NAME, calendar.getTime());
		ApplicationInstance applicationInstance2 = this.applicationDao.getApplicationInstance(APP_NAME, calendar2.getTime());
		Set<Long> timestamps = new HashSet<Long>();
		timestamps.add(new Date().getTime());
		Set<ApplicationInstance> dbInstances = this.applicationDao.getApplicationInstances(APP_NAME, timestamps);
		assertEquals(0, dbInstances.size());
		Set<ApplicationInstance> testSet = new HashSet<ApplicationInstance>();
		testSet.add(applicationInstance1);
		timestamps.add(calendar.getTimeInMillis());
		dbInstances = this.applicationDao.getApplicationInstances(APP_NAME, timestamps);
		assertEquals(testSet, dbInstances);
		testSet.add(applicationInstance2);
		timestamps.add(calendar2.getTimeInMillis());
		dbInstances = this.applicationDao.getApplicationInstances(APP_NAME, timestamps);
		assertEquals(testSet, dbInstances);
	}
	
	
	
}
