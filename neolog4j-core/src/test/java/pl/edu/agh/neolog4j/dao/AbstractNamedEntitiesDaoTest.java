package pl.edu.agh.neolog4j.dao;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.dao.DataIntegrityViolationException;

import pl.edu.agh.neolog4j.AbstractTest;
import pl.edu.agh.neolog4j.model.AbstractModelClass;
import pl.edu.agh.neolog4j.model.UniqueNamedNodesInterface;

public abstract class AbstractNamedEntitiesDaoTest<T extends UniqueNamedNodesInterface> extends AbstractTest {

	protected static final String TEST_NAME = "testName";
	
	protected abstract T getNode(String p_name);
	protected abstract Class<T> getProcessedEntityClass();
	protected abstract AbstractDao getAbstractDao();
	
	@Test
	public void getNodeTest() {
		assertEquals(0, getAbstractDao().getTemplate().count(getProcessedEntityClass()));
		T node1 = getNode(TEST_NAME);
		T node2 = getNode(TEST_NAME);
		assertEquals(node1, node2);
		assertEquals(1, getAbstractDao().getTemplate().count(getProcessedEntityClass()));
	}
	
	
	@Test
	public void nameChangeTest() {
		assertEquals(0, getAbstractDao().getTemplate().count(getProcessedEntityClass()));
		T node = getNode(TEST_NAME);
		assertEquals(1, getAbstractDao().getTemplate().count(getProcessedEntityClass()));
		String changedName = TEST_NAME + "_changed";
		node.setName(changedName);
		changedName = node.getName();
		getAbstractDao().getTemplate().save(node);
		assertEquals(1, getAbstractDao().getTemplate().count(getProcessedEntityClass()));
		Long graphID = ((AbstractModelClass) node).getGraphID();
		T found = getAbstractDao().getTemplate().findOne(graphID, getProcessedEntityClass());
		assertEquals(changedName, found.getName());
	}
	
	@Test(expected = DataIntegrityViolationException.class)
	public void nameChangedExcTest() {
		assertEquals(0, getAbstractDao().getTemplate().count(getProcessedEntityClass()));
		String secondName = TEST_NAME + "_changed";
		getNode(TEST_NAME);
		T node2 = getNode(secondName);
		assertEquals(2, getAbstractDao().getTemplate().count(getProcessedEntityClass()));
		node2.setName(TEST_NAME);
		getAbstractDao().getTemplate().save(node2);
	}
}
