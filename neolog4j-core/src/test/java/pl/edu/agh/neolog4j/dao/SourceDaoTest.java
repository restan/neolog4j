package pl.edu.agh.neolog4j.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import pl.edu.agh.neolog4j.model.SourceClass;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:META-INF/spring/test-applicationContext.xml")
public class SourceDaoTest extends AbstractNamedEntitiesDaoTest<SourceClass> {

	protected static final String PACKAGE_1_TEST = "package.test1";
	protected static final String PACKAGE_2_TEST = "package.test2";
	protected static final String PACKAGE_3_TEST = "test2";
	protected static final String CLASS_1_TEST = "Class1";
	protected static final String CLASS_2_TEST = "Class2";
	
	@Autowired protected SourceDao sourceDao;
	
	
	@Test
	public void defaultPackageClassTest() {
		SourceClass sourceClass = this.sourceDao.getSource(CLASS_1_TEST);
		SourceClass sourceClass2 = this.sourceDao.getSource(CLASS_1_TEST);
		assertEquals(sourceClass, sourceClass2);
		assertEquals(sourceClass.getParentPackage(), sourceClass2.getParentPackage());
	}
	
	
	@Test
	public void equalClassTest() {
		SourceClass sourceClass = this.sourceDao.getSource(PACKAGE_1_TEST + "." + CLASS_1_TEST);
		SourceClass sourceClass2 = this.sourceDao.getSource(PACKAGE_1_TEST + "." + CLASS_1_TEST);
		assertEquals(sourceClass, sourceClass2);
		assertEquals(sourceClass.getParentPackage(), sourceClass2.getParentPackage());
	}
	
	@Test
	public void differentPackagesTest() {
		SourceClass sourceClass = this.sourceDao.getSource(PACKAGE_1_TEST + "." + CLASS_1_TEST);
		SourceClass sourceClass2 = this.sourceDao.getSource(PACKAGE_2_TEST + "." + CLASS_1_TEST);
		assertNotEquals(sourceClass, sourceClass2);
		assertNotEquals(sourceClass.getParentPackage(), sourceClass2.getParentPackage());
	}
	
	@Test
	public void differenClassNameSamePackageTest() {
		SourceClass sourceClass = this.sourceDao.getSource(PACKAGE_1_TEST + "." + CLASS_1_TEST);
		SourceClass sourceClass2 = this.sourceDao.getSource(PACKAGE_1_TEST + "." + CLASS_2_TEST);
		assertNotEquals(sourceClass, sourceClass2);
		assertEquals(sourceClass.getParentPackage(), sourceClass2.getParentPackage());
	}
	
	@Test
	public void differentPackageHierarchyTest() {
		SourceClass sourceClass = this.sourceDao.getSource(PACKAGE_1_TEST + "." + CLASS_1_TEST);
		SourceClass sourceClass2 = this.sourceDao.getSource(PACKAGE_3_TEST + "." + CLASS_1_TEST);
		assertNotEquals(sourceClass, sourceClass2);
		assertNotEquals(sourceClass.getParentPackage(), sourceClass2.getParentPackage());
	}


	@Override
	protected SourceClass getNode(String p_name) {
		return this.sourceDao.getSource(p_name);
	}


	@Override
	protected Class<SourceClass> getProcessedEntityClass() {
		return SourceClass.class;
	}


	@Override
	protected AbstractDao getAbstractDao() {
		return this.sourceDao;
	}
	
	
}
