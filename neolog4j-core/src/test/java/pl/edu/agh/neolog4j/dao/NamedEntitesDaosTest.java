package pl.edu.agh.neolog4j.dao;

import static org.junit.Assert.assertNotEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import pl.edu.agh.neolog4j.AbstractTest;
import pl.edu.agh.neolog4j.model.Application;
import pl.edu.agh.neolog4j.model.SourceClass;
import pl.edu.agh.neolog4j.model.SourcePackage;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:META-INF/spring/test-applicationContext.xml")
public class NamedEntitesDaosTest extends AbstractTest {

	protected static final String NAME = "testName_p1.testName_p2";
	
	@Autowired protected ApplicationDao applicationDao;
	@Autowired protected SourceDao sourceDao;
	
	@Test
	public void differentEntitiesSameName() {
		Application application = this.applicationDao.getApplication(NAME);
		SourceClass source = this.sourceDao.getSource(NAME);
		SourcePackage sourcePackage = this.sourceDao.getSourcePackage(NAME);
		assertNotEquals(application, source);
		assertNotEquals(application, sourcePackage);
		assertNotEquals(sourcePackage, source);
	}
	
}
