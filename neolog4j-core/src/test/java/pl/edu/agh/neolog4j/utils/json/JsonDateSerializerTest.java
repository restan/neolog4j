package pl.edu.agh.neolog4j.utils.json;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.map.SerializerProvider;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import pl.edu.agh.neolog4j.utils.json.JsonDateSerializer;

public class JsonDateSerializerTest {
	
	@Mock
    private JsonGenerator jsonGenerator;
	
	@Mock
    private SerializerProvider serializerProvider;
	
	private JsonDateSerializer jsonDateSerializer;
	
	@Before
	public void setUp() throws Exception {
		jsonGenerator = Mockito.mock(JsonGenerator.class);
		serializerProvider = Mockito.mock(SerializerProvider.class);
		jsonDateSerializer = new JsonDateSerializer();
	}
	
	@Test(expected = NullPointerException.class)
	public void testSerializeThrowException() throws Exception {
		Date date = null;
		jsonDateSerializer.serialize(date, jsonGenerator, serializerProvider);
	}
	
	@Test
	public void testSerialize() throws Exception {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String stringDate = sdf.format(date);
		
		jsonDateSerializer.serialize(date, jsonGenerator, serializerProvider);
		Mockito.verify(jsonGenerator, Mockito.times(1)).writeString(stringDate);
	}

}
