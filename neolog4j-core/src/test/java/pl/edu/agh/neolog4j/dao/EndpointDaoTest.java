package pl.edu.agh.neolog4j.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import pl.edu.agh.neolog4j.model.Application;
import pl.edu.agh.neolog4j.model.Endpoint;
import pl.edu.agh.neolog4j.model.InEndpoint;
import pl.edu.agh.neolog4j.model.OutEndpoint;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:META-INF/spring/test-applicationContext.xml")
public class EndpointDaoTest extends AbstractNamedEntitiesDaoTest<Endpoint>{

	protected static final String IN_ENDP_NAME = "IN";
	protected static final String OUT_ENDP_NAME = "OUT";
	protected static final String APP_NAME = "APP_NAME";
	
	@Autowired
	protected EndpointDao endpointDao;
	
	@Autowired
	protected ApplicationDao applicationDao;
	
	@Override
	protected Endpoint getNode(String p_name) {
		return this.endpointDao.getOutEndpoint(p_name, APP_NAME);
	}

	@Override
	protected Class<Endpoint> getProcessedEntityClass() {
		return Endpoint.class;
	}

	@Override
	protected AbstractDao getAbstractDao() {
		return this.endpointDao;
	}
	
	@Test
	public void testGetEndpoint() {
		Endpoint endpoint = this.endpointDao.getInEndpoint(IN_ENDP_NAME, APP_NAME);
		Endpoint requery = this.endpointDao.requery(endpoint, Endpoint.class);
		assertNotNull(requery.getGraphID());
		Endpoint endpoint2 = this.endpointDao.getInEndpoint(IN_ENDP_NAME, APP_NAME);
		assertEquals(endpoint, endpoint2);
		Set<Endpoint> allEndpoints = this.endpointDao.getAllNodes(Endpoint.class);
		assertEquals(1, allEndpoints.size());
		OutEndpoint endpoint3 = this.endpointDao.getOutEndpoint(OUT_ENDP_NAME, APP_NAME);
		assertNotEquals(endpoint, endpoint3);
		OutEndpoint endpoint4 = this.endpointDao.getOutEndpoint(OUT_ENDP_NAME, APP_NAME);
		assertEquals(endpoint3, endpoint4);
		allEndpoints = this.endpointDao.getAllNodes(Endpoint.class);
		assertEquals(2, allEndpoints.size());
		Application application = this.applicationDao.getApplication(APP_NAME);
		assertEquals(application, endpoint.getApplication());
		assertEquals(application, endpoint3.getApplication());	
	}
	
	@Test
	public void testConnectEndpoints() {
		OutEndpoint out = this.endpointDao.getOutEndpoint(OUT_ENDP_NAME, APP_NAME);
		InEndpoint in = this.endpointDao.getInEndpoint(IN_ENDP_NAME, APP_NAME);
		this.endpointDao.connect(in, out);
		InEndpoint in2 = this.endpointDao.getInEndpoint(IN_ENDP_NAME + "_2", APP_NAME);
		this.endpointDao.connect(in2, out);
		OutEndpoint out2 = this.endpointDao.getOutEndpoint(OUT_ENDP_NAME + "_2", APP_NAME);
		this.endpointDao.connect(in, out2);
		Set<OutEndpoint> testInOuts = new HashSet<OutEndpoint>();
		testInOuts.add(out);
		testInOuts.add(out2);
		Set<OutEndpoint> testIn2Outs = new HashSet<OutEndpoint>();
		testIn2Outs.add(out);
		Set<InEndpoint> testOutIns = new HashSet<InEndpoint>();
		testOutIns.add(in);
		testOutIns.add(in2);
		Set<InEndpoint> testOut2Ins = new HashSet<InEndpoint>();
		testOut2Ins.add(in);
		in = this.endpointDao.requery(in, InEndpoint.class);
		in2 = this.endpointDao.requery(in2, InEndpoint.class);
		out = this.endpointDao.requery(out, OutEndpoint.class);
		out2 = this.endpointDao.requery(out2, OutEndpoint.class);
		assertEquals(in.getSources(), testInOuts);
		assertEquals(in2.getSources(), testIn2Outs);
		assertEquals(out.getDestinations(), testOutIns);
		assertEquals(out2.getDestinations(), testOut2Ins);
		
	}
	
	@Test
	public void testDisconnectNodes() {
		OutEndpoint out = this.endpointDao.getOutEndpoint(OUT_ENDP_NAME, APP_NAME);
		InEndpoint in = this.endpointDao.getInEndpoint(IN_ENDP_NAME, APP_NAME);
		OutEndpoint out2 = this.endpointDao.getOutEndpoint(OUT_ENDP_NAME + "_2", APP_NAME);
		this.endpointDao.connect(in, out);
		this.endpointDao.connect(in, out2);
		OutEndpoint connectedOut = this.endpointDao.requery(out, OutEndpoint.class);
		InEndpoint connectedIn = this.endpointDao.requery(in, InEndpoint.class);
		assertEquals(1, connectedOut.getDestinations().size());
		assertEquals(2, connectedIn.getSources().size());
		this.endpointDao.disconnectEndpoints(in, out);
		OutEndpoint disconnectedOut = this.endpointDao.requery(out, OutEndpoint.class);
		InEndpoint disconnectedIn = this.endpointDao.requery(in, InEndpoint.class);
		assertEquals(0, disconnectedOut.getDestinations().size());
		assertEquals(1, disconnectedIn.getSources().size());
		OutEndpoint secondConnected = this.endpointDao.getOutEndpoint(OUT_ENDP_NAME + "_2", APP_NAME);
		assertEquals(1, secondConnected.getDestinations().size());
	}
}
