<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<base href="${pageContext.request.contextPath}/" />
		<title>NeoLog4j Console</title>
		<link rel="stylesheet" href="css/960.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="css/template.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="css/colour.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="css/configuration.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="css/logviewer.css" type="text/css" media="screen" charset="utf-8" />
		
<!-- 		<link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" /> -->
		
		<link rel="stylesheet" href="css/ui-lightness/jquery-ui.css" type="text/css" media="screen" charset="utf-8" />
		
		<script src="js/glow/1.7.0/core/core.js" type="text/javascript"></script>
		<script src="js/glow/1.7.0/widgets/widgets.js" type="text/javascript"></script>
		<link href="js/glow/1.7.0/widgets/widgets.css" type="text/css" rel="stylesheet" />
<!-- 		<script src="js/jquery/jquery-1.9.1.js" type="text/javascript"></script> -->
<!-- 		<script src="js/jquery/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script> -->
		<link href="css/ui-lightness/jquery-ui-timepicker-addon.css" type="text/css" rel="stylesheet" />
		
		<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
		<script src="js/timepicker/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
		
		<script src="js/simplemodal/jquery.simplemodal-1.4.4.js" type="text/javascript"></script>
		<link href="css/simplemodal.css" type="text/css" rel="stylesheet" />
		
		<script src="js/sigma/sigma.concat.js"></script>
		<script src="js/sigma/plugins/sigma.parseGexf.js"></script>
		<script src="js/sigma/plugins/sigma.forceatlas2.js"></script>
		<link href="css/sigma.css" type="text/css" rel="stylesheet" />
		
	</head>
	<body>

		<h1 id="head">NeoLog4j Console</h1>
		<ul id="navigation">
			<li>
				<a href="configuration" class="">
					Configuration
				</a>
			</li>
			<li>
				<a href="logviewer"class="">
					Log Viewer
				</a>
			</li>
		</ul>