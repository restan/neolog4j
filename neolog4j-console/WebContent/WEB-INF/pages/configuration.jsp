<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<jsp:include page="header.jsp" />
<div id="content-databases" class="container_16 clearfix content-box">
	<script type="text/javascript">
		var databases = [
		<c:forEach items="${databases}" var="database">
		    '${database.url}',
		</c:forEach>
		];
		
		function addError(selector, error){
			$(selector + ' h3').after('<p class="error">'+error+'</p>');
		}
		
		function addDatabase(url, testConnection){
			cleanErrors();
			$.ajax({
				'type': "post",
				'url': "configuration/ajax/addDatabase",
				'data': {
					'url': url,
					'testConnection': testConnection
				}
			}).error(function(jqXHR, textStatus, errorThrown){
				var error = "ERROR: " + errorThrown;
				addError('.add_database_dialog', error);
			}).success(function(data){
				console.log(data);
				if(data['status'] == 'ERROR'){
					var error = "ERROR: " + data['error'];
					addError('.add_database_dialog', error);
				}
				else{
					var url = data['result']['url'];
					var lastSynchronization = data['result']['lastSynchronization'] || '---';
					$('#databases table tbody').append(
						'<tr database_url="'+url+'">\
							<td>'+url+'</td>\
							<td>'+lastSynchronization+'</td>\
							<td><a class="synchronize database_synchronize">Synchronize</a></td>\
							<td><a class="delete database_delete">Delete</a></td>\
						</tr>');
					databases.push(url);
					bindEvents();
					closeDialogs();
				}
			});
		}
		
		function deleteDatabase(url){
			cleanErrors();
			$.ajax({
				'type': "post",
				'url': "configuration/ajax/deleteDatabase",
				'data': {
					'url': url
				}
			}).error(function(jqXHR, textStatus, errorThrown){
				var error = "ERROR: " + errorThrown;
				addError('.delete_database_dialog', error);
			}).success(function(data){
				console.log(data);
				if(data['status'] == 'ERROR'){
					var error = "ERROR: " + data['error'];
					addError('.delete_database_dialog', error);
				}
				else{
					$('tr[database_url="'+url+'"]').remove();
					closeDialogs();
				}
			});
		}
		
		function synchronizeDatabase(url){
			cleanErrors();
			$.ajax({
				'type': "post",
				'url': "configuration/ajax/synchronizeDatabase",
				'data': {
					'url': url
				}
			}).error(function(jqXHR, textStatus, errorThrown){
				var error = "ERROR: " + errorThrown;
				addError('.synchronize_database_dialog', error);
			}).success(function(data){
				console.log(data);
				if(data['status'] == 'ERROR'){
					var error = "ERROR: " + data['error'];
					addError('.synchronize_database_dialog', error);
				}
				else{
					var url = data['result']['url'];
					var lastSynchronization = data['result']['lastSynchronization'];
					$($('tr[database_url="'+url+'"] td')[1]).text(lastSynchronization);
					closeDialogs();
				}
			});
		}

		function synchronizeDatabases(){
			cleanErrors();
			var counter = databases.length;
			for (var i = 0; i < databases.length; i++) {
			    url = databases[i];
				$.ajax({
					'type': "post",
					'url': "configuration/ajax/synchronizeDatabase",
					'data': {
						'url': url,
					}
				}).error(function(jqXHR, textStatus, errorThrown){
					var error = "ERROR: " + errorThrown;
					addError('.synchronize_database_dialog', error);
				}).success(function(data){
					console.log(data);
					if(data['status'] == 'ERROR'){
						var error = "ERROR: " + data['error'];
						addError('.synchronize_databases_dialog', error);
					}
					else{
						var url = data['result']['url'];
						var lastSynchronization = data['result']['lastSynchronization'];
						$($('tr[database_url="'+url+'"] td')[1]).text(lastSynchronization);
						counter--;
						if(counter == 0){
							closeDialogs();
						}
					}
				});
			}
		}
		
		function closeDialogs(){
			$.modal.close();
			cleanErrors();
		}
		
		function cleanErrors(){
			$('.modal_dialog .error').remove();
		}
		
		function bindEvents(){
			$('.database_synchronize').click(function(){
				var url = $(this).parents('tr').attr('database_url');
				$('.synchronize_database_dialog .database_url').text(url);
				$(".synchronize_database_dialog").modal();
			});
			$('.database_delete').click(function(){
				var url = $(this).parents('tr').attr('database_url');
				$('.delete_database_dialog .database_url').text(url);
				$(".delete_database_dialog").modal();
			});
		}
		
		$(function(){
			$('.database_add').click(function(){
				$(".add_database_dialog").modal();
			});
			$('.database_synchronize_all').click(function(){
				$(".synchronize_databases_dialog").modal();
			});
			
			$('.cancel_button').click(function(){
				closeDialogs();
			});
			$('.add_database_dialog .ok_button').click(function(){
				var url = $('#database_url').val();
				var testConnection = $('#database_test_connection').is(':checked');
				addDatabase(url, testConnection);
			});
			$('.delete_database_dialog .ok_button').click(function(){
				var url = $('.delete_database_dialog .database_url').text();
				deleteDatabase(url);
			});
			$('.synchronize_database_dialog .ok_button').click(function(){
				var url = $('.synchronize_database_dialog .database_url').text();
				synchronizeDatabase(url);
			});
			$('.synchronize_databases_dialog .ok_button').click(function(){
				synchronizeDatabases();
			});
			
			bindEvents();
		});
	</script>
	
	<div class="modal_dialog add_database_dialog">
		<h3>Add Database</h3>
		<p>
			<label>Database url</label>
			<input id="database_url" type="text"/>
		</p>
		<p>
			<label>Test connection</label>
			<input id="database_test_connection" type="checkbox"/>
		</p>
		<p class="buttons">
			<input class="cancel_button" type="reset" value="Cancel"/>
			<input class="ok_button" type="submit" value="OK"/>
		</p>
	</div>
	
	<div class="modal_dialog delete_database_dialog">
		<h3>Delete Database</h3>
		<p>
			Delete <span class="database_url"></span>?
		</p>
		<p class="buttons">
			<input class="cancel_button" type="reset" value="Cancel"/>
			<input class="ok_button" type="submit" value="OK"/>
		</p>
	</div>
	
	<div class="modal_dialog synchronize_database_dialog">
		<h3>Synchronize Database</h3>
		<p>
			Synchronize <span class="database_url"></span>?
		</p>
		<p class="buttons">
			<input class="cancel_button" type="reset" value="Cancel"/>
			<input class="ok_button" type="submit" value="OK"/>
		</p>
	</div>
	
	<div class="modal_dialog synchronize_databases_dialog">
		<h3>Synchronize Databases</h3>
		<p>
			Synchronize all databases?
		</p>
		<p class="buttons">
			<input class="cancel_button" type="reset" value="Cancel"/>
			<input class="ok_button" type="submit" value="OK"/>
		</p>
	</div>
	
	<div class="header">
		<h4>
			Databases
		</h4>
	</div>
	<div id="databases" class="content">
		<div class="grid_16">
			<table>
				<thead>
					<tr>
						<th>Url</th>
						<th>Last Synchronization</th>
						<th colspan="2" width="10%">Actions</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="5" class="pagination">
							<a class="add database_add">Add</a>
							<a class="synchronize database_synchronize_all">Synchronize All</a>
						</td>
					</tr>
				</tfoot>
				<tbody>
					<c:forEach items="${databases}" var="database">
					<tr database_url="${database.url}">
						<td>${database.url}</td>
						<td>
							<c:choose>
								<c:when test="${database.lastSynchronization != null}">
									<fmt:formatDate value="${database.lastSynchronization}" pattern="yyyy-MM-dd hh:mm:ss"/>
								</c:when>
								<c:otherwise>
									---
								</c:otherwise>
							</c:choose>
						</td>
						<td><a class="synchronize database_synchronize">Synchronize</a></td>
						<td><a class="delete database_delete">Delete</a></td>
					</tr>
					
					</c:forEach>
				</tbody>
				
			</table>
		</div>
	</div>
</div>

<div id="content-connection_graph" class="container_16 clearfix content-box">
	<script type="text/javascript">
		var sigInst = null;
		var greyColor = '#666';
		var state = 0;
		var outEndpointNode = null;
		var inEndpointNode = null;
		var colorize = 0;
		
		function StringtoXML(text){
            if (window.ActiveXObject){
              var doc=new ActiveXObject('Microsoft.XMLDOM');
              doc.async='false';
              doc.loadXML(text);
            } else {
              var parser=new DOMParser();
              var doc=parser.parseFromString(text,'text/xml');
            }
            return doc;
        }
		
		function getNodeType(node){
			for (var i = 0; i < node.attr.attributes.length; i++) {
				if(node.attr.attributes[i].attr == 'type'){
					return node.attr.attributes[i].val;
				};
			};
		}
		
		function showEndpointConnectionDialog(){
			var inApp, inEp, outApp, outEp;
			for (var i = 0; i < inEndpointNode.attr.attributes.length; i++) {
				if(inEndpointNode.attr.attributes[i].attr == 'appName'){
					inApp = inEndpointNode.attr.attributes[i].val;
				} else if(inEndpointNode.attr.attributes[i].attr == 'name'){
					inEp = inEndpointNode.attr.attributes[i].val;
				};
			};
			for (var i = 0; i < outEndpointNode.attr.attributes.length; i++) {
				if(outEndpointNode.attr.attributes[i].attr == 'appName'){
					outApp = outEndpointNode.attr.attributes[i].val;
				} else if(outEndpointNode.attr.attributes[i].attr == 'name'){
					outEp = outEndpointNode.attr.attributes[i].val;
				};
			};
			$('#connect_inendpoint').text(inEp);
			$('#connect_inapp').text(inApp);
			$('#connect_outendpoint').text(outEp);
			$('#connect_outapp').text(outApp);
			$(".connect_endpoints_dialog").modal();
		}
		
		function showEndpointDisconnectionDialog(){
			var inApp, inEp, outApp, outEp;
			for (var i = 0; i < inEndpointNode.attr.attributes.length; i++) {
				if(inEndpointNode.attr.attributes[i].attr == 'appName'){
					inApp = inEndpointNode.attr.attributes[i].val;
				} else if(inEndpointNode.attr.attributes[i].attr == 'name'){
					inEp = inEndpointNode.attr.attributes[i].val;
				};
			};
			for (var i = 0; i < outEndpointNode.attr.attributes.length; i++) {
				if(outEndpointNode.attr.attributes[i].attr == 'appName'){
					outApp = outEndpointNode.attr.attributes[i].val;
				} else if(outEndpointNode.attr.attributes[i].attr == 'name'){
					outEp = outEndpointNode.attr.attributes[i].val;
				};
			};
			$('#disconnect_inendpoint').text(inEp);
			$('#disconnect_inapp').text(inApp);
			$('#disconnect_outendpoint').text(outEp);
			$('#disconnect_outapp').text(outApp);
			$(".disconnect_endpoints_dialog").modal();
		}
		
		function connectEndpoints(inApp, inEp, outApp, outEp){
			cleanErrors();
			$.ajax({
				'type': "post",
				'url': "configuration/ajax/connectEndpoints",
				'data': {
					'outApplication': outApp,
					'outEndpoint': outEp,
					'inApplication': inApp,
					'inEndpoint': inEp,
				}
			}).error(function(jqXHR, textStatus, errorThrown){
				console.log(jqXHR, textStatus, errorThrown);
				var error = "ERROR: " + errorThrown;
				addError('.connect_endpoints_dialog', error);
			}).success(function(data){
				console.log(data);
				if(data['status'] == 'ERROR'){
					var error = "ERROR: " + data['error'];
					addError('.connect_endpoints_dialog', error);
				}
				else{
					reloadGraph();
					closeDialogs();
				}
			});
		}
		
		function disconnectEndpoints(inApp, inEp, outApp, outEp){
			cleanErrors();
			$.ajax({
				'type': "post",
				'url': "configuration/ajax/disconnectEndpoints",
				'data': {
					'outApplication': outApp,
					'outEndpoint': outEp,
					'inApplication': inApp,
					'inEndpoint': inEp,
				}
			}).error(function(jqXHR, textStatus, errorThrown){
				console.log(jqXHR, textStatus, errorThrown);
				var error = "ERROR: " + errorThrown;
				addError('.disconnect_endpoints_dialog', error);
			}).success(function(data){
				console.log(data);
				if(data['status'] == 'ERROR'){
					var error = "ERROR: " + data['error'];
					addError('.disconnect_endpoints_dialog', error);
				}
				else{
					reloadGraph();
					closeDialogs();
				}
			});
		}
		
		function init(){
			var sigRoot = document.getElementById('connection_graph');
			sigInst = sigma.init(sigRoot).drawingProperties({
				defaultLabelColor: '#333',
				font: 'Arial',
				edgeColor: '#000',
				defaultEdgeColor: '#000',
				defaultEdgeType: 'curve',
				borderSize: 1,
			}).graphProperties({
				minNodeSize: 1,
				maxNodeSize: 10,
				minEdgeSize: 4,
				maxEdgeSize: 4,
			}).mouseProperties({
				maxRatio: 10,
				minRatio: 1,
			});

			sigInst.bind('downnodes',function(event){
				colorize = 0;
				console.log(event);
				var node = null;
			    sigInst.iterNodes(function(n){
			        node = n;
			    },[event.content[0]]);
			    
			    if(getNodeType(node) == 'outEndpoint'){
			    	if(state==0){
			    		// start connecting
			    		outEndpointNode = node;
						sigInst.iterNodes(function(n){
							if(n.id != event.content[0] && getNodeType(n) != 'inEndpoint'){
								if(!n.attr['grey']){
							        n.attr['true_color'] = n.color;
							        n.color = greyColor;
							        n.attr['grey'] = 1;
							    }
							}
					    }).draw();
					
						state = 1;
			    	}
			    	else if(state==2){
			    		// finish disconnecting 
			    	}
			    };

			    if(getNodeType(node) == 'inEndpoint'){
			    	if(state==0){
			    		// start disconnecting 
			    		inEndpointNode = node;
			    		var nodes = event.content;
			    	    var neighbors = {};
			    	    sigInst.iterEdges(function(e){
			    	    	if(!(nodes.indexOf(e.source)<0 && nodes.indexOf(e.target)<0)){
			    	    		neighbors[e.source] = 1;
			    	            neighbors[e.target] = 1;
			    	    	}
			    	    }).iterNodes(function(n){
							if(n.id != event.content[0] && (getNodeType(n) != 'outEndpoint' || !neighbors[n.id])){
								if(!n.attr['grey']){
							        n.attr['true_color'] = n.color;
							        n.color = greyColor;
							        n.attr['grey'] = 1;
							    }
							}
					    }).draw();
					
						state = 2;
			    	}
			    	else if(state==1){
			    		// finish connecting 
			    		inEndpointNode = node;
			    		showEndpointConnectionDialog();
			    	}
			    };
			});
			
			sigInst.bind('downgraph',function(event){
				console.log(colorize,event);
				colorize = 1;
			});
			
			sigInst.bind('upgraph',function(event){
				console.log(colorize,event);
				if(colorize && state!=0){
					// stop actions 
					stopActions();
				}
			});
		}
		
		function stopActions(){
    	    sigInst.iterNodes(function(n){
    	    	if(n.attr['grey']){
			        n.color = n.attr['true_color'];
			        n.attr['grey'] = 0;
			    }
    	    }).draw();
    	    
			state = 0;
		}
		
		function drawGraph(gephi){
			gephi = StringtoXML(gephi);
			sigInst.emptyGraph();
			sigInst.parseGexf(gephi);
			sigInst.startForceAtlas2();
			sigInst.draw();
			setTimeout(function(){sigInst.stopForceAtlas2();}, 7500);
			state = 0;
		}
		
		function reloadGraph(){
			cleanErrors();
			$.ajax({
				'type': "get",
				'url': "configuration/ajax/configurationGraph"
			}).error(function(jqXHR, textStatus, errorThrown){
				var error = "ERROR: " + errorThrown;
				console.log(error);
				// TODO
			}).success(function(data){
				console.log(data);
				if(data['status'] == 'ERROR'){
					
				}
				else{
					gephi = data['result'];
					drawGraph(gephi);
				}
			});
		}
		
		$(function(){
			init();
			$('#graph_reloader').click(function(){
				reloadGraph();
			});
			
			$('.connect_endpoints_dialog .cancel_button, .disconnect_endpoints_dialog .cancel_button').click(function(){
				stopActions();
				closeDialogs();
			});
			$('.connect_endpoints_dialog .ok_button').click(function(){

				inEp = $('#connect_inendpoint').text();
				inApp = $('#connect_inapp').text();
				outEp = $('#connect_outendpoint').text();
				outApp = $('#connect_outapp').text();
				connectEndpoints(inApp, inEp, outApp, outEp);
			});
			$('.disconnect_endpoints_dialog .ok_button').click(function(){

				inEp = $('#disconnect_inendpoint').text();
				inApp = $('#disconnect_inapp').text();
				outEp = $('#disconnect_outendpoint').text();
				outApp = $('#disconnect_outapp').text();
				disconnectEndpoints(inApp, inEp, outApp, outEp);
			});
		});
	</script>
	
	<div class="modal_dialog connect_endpoints_dialog">
		<h3>Connect Endpoints</h3>
		<p>
			Connect endpoint <span id="connect_inendpoint"></span> (<span id="connect_inapp"></span>)
			to endpoint <span id="connect_outendpoint"></span> (<span id="connect_outapp"></span>)?
		</p>
		<p class="buttons">
			<input class="cancel_button" type="reset" value="Cancel"/>
			<input class="ok_button" type="submit" value="OK"/>
		</p>
	</div>
	
	<div class="modal_dialog disconnect_endpoints_dialog">
		<h3>Disconnect Endpoints</h3>
		<p>
			Disconnect endpoint <span id="disconnect_inendpoint"></span> (<span id="disconnect_inapp"></span>)
			from endpoint <span id="disconnect_outendpoint"></span> (<span id="disconnect_outapp"></span>)?
		</p>
		<p class="buttons">
			<input class="cancel_button" type="reset" value="Cancel"/>
			<input class="ok_button" type="submit" value="OK"/>
		</p>
	</div>
	
	<div class="header">
		<h4>
			Connection Graph
		</h4>
		<div id="graph_reloader"></div>
	</div>
	<div class="content">
		<div id="connection_graph" class="sig">
		
		</div>
	</div>
</div>
<jsp:include page="footer.jsp" />