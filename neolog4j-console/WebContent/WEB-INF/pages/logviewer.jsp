<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<jsp:include page="header.jsp"/>
<div id="content-filter" class="container_16 clearfix content-box">

	<script type="text/javascript">
	var visibility = !${f};
	function update_view(){
		if(visibility){
			$('#logviewer_filter').slideDown(300);
		}
		else{
			$('#logviewer_filter').slideUp(300);
		}
		$('#filter_visibility_switch').
			css('background-image', visibility ? 'url(images/arrows_up.png)' : 'url(images/arrows_down.png)');
	}
	
	$(function(){
		update_view();
		$('#filter_visibility_switch').click(function(){
			visibility = !visibility;
			update_view();
		});
		$('#before').datetimepicker({
			dateFormat: 'yy-mm-dd',
			timeFormat: 'HH:mm:ss',
		});
		$('#after').datetimepicker({
			dateFormat: 'yy-mm-dd',
			timeFormat: 'HH:mm:ss',
		});
	});
	</script>
	
	<div class="header">
		<h4>Filter</h4>
		<div id="filter_visibility_switch"></div>
	</div>
	<div id="logviewer_filter" class="content" style="display:none;">
		<form:form class="spring_form" method="GET" commandName="query">
			<div class="grid_4">
				<p>
					<label for="levels">Level</label>
					<c:forEach var="item" items="${logLevels}">
    					<span class="checkbox">
    						<form:checkbox label="${item}" path="levels" value="${item}"/>
    					</span>
					</c:forEach>
				</p>
			</div>
			<div class="grid_4">
				<p>
					<label for="orderBy">Order By</label>
					<form:select path="orderType">
						<form:options items="${orderTypes}"/>
					</form:select>
					<c:forEach var="item" items="${orderFields}">
    					<span class="radiobutton">
    						<form:checkbox label="${item}" path="orderBy" value="${item}"/>
    					</span>
					</c:forEach>
				</p>
				<p>
					<label for="offset">Offset</label>
					<form:input path="offset"/>
				</p>
				<p>
					<label for="limit">Limit</label>
					<form:input path="limit"/>
				</p>
			</div>
			<div class="grid_4">
				<p>
					<label for="after">After</label>
					<form:input path="after"/>
				</p>
				<p>
					<label for="before">Before</label>
					<form:input path="before"/>
				</p>
			</div>
			<div class="grid_4">
				<p>
					<label for="applicationInstances">Applications</label>
					<c:forEach var="item" items="${applicationInstances}">
    					<span class="radiobutton">
    						<c:set var="checked" value="false"/>
    						<c:forEach var="inst" items="${query.applicationInstances}">
    						    <c:if test="${inst.graphID == item.graphID}">
    								<c:set var="checked" value="true"/>
    						    </c:if>
    						</c:forEach> 
    						<input id="applicationInstances${item.graphID}" name="applicationInstances" type="checkbox" value="${item.graphID}" ${checked ? "checked=\"checked\"": ""}/>
    						<label for="applicationInstances${item.graphID}">
    							${item.application.name}
    						</label>
    						<input type="hidden" name="_applicationInstances" value="on"/>
    						<br/>
    						<small style="margin-left:18px;">
    							<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${item.createDate}" />
    						</small>
    					</span>
					</c:forEach>
				</p>
				<input type="submit" value="Submit" style="float:right; margin-bottom:10px;"/>
			</div>
			<input type="hidden" name="f" value="1" />
		</form:form>
	</div>
</div>

<c:if test="${f}">
<div id="content-logevents" class="container_16 clearfix content-box">
	
	<script type="text/javascript">

		var sigInst = null;
	    var popUp;
	    var rootNode = null;
		var minTimestamp = null;
		var maxTimestamp = null;
		var logColors = {
			TRACE : '#BDDBFF',
			DEBUG : '#AFE9AF',
			INFO : '#FFFB5B',
			WARN : '#FFBFBF',
			ERROR : '#FC3939',
			FATAL : '#FF0000',
		};
		var borderColors = {
			NORMAL : '#888',
			IN_ENDPOINT : '#005091',
			OUT_ENDPOINT : '#85e0ff',
		};
		var proposalEdgeColor = '#999';
		var edgeColor = '#555';
		
		function openLogPath(eventId, databaseId) {
			console.log(eventId, databaseId);
			if (sigInst)
				sigInst.emptyGraph();
			$(".log_path_dialog").modal({
				containerCss : {
					width : '90%',
					height : '90%',
				},
			});
			loadLogPath(eventId, databaseId);
		}
		
		function init() {
			var sigRoot = document.getElementById('log_path_graph');
			sigInst = sigma.init(sigRoot).drawingProperties({
				defaultEdgeColor : edgeColor,
				borderSize : 2,
				defaultEdgeArrow : 'target',
				defaultBorderView: 'node',
			}).graphProperties({
				minNodeSize : 1,
				maxNodeSize : 10,
				minEdgeSize : 4,
				maxEdgeSize : 4,
			}).mouseProperties({
				maxRatio : 10,
				minRatio : 1,
			});
			sigInst.bind('overnodes',showNodeInfo).bind('outnodes',hideNodeInfo);

			sigInst.bind('downnodes',function(event){
				var node = null;
			    sigInst.iterNodes(function(n){
			        node = n;
			    },[event.content[0]]);
				
			    var loaded = false;
			    if(isLastNode(node) && !node['attr']['loadedNext']){
			    	loadNext(node, false);
			    	node['attr']['loadedNext'] = true;
			    	loaded = true;
			    }
			    if(isFirstNode(node) && !node['attr']['loadedPrev']){
					loadPrevious(node, false);
			    	node['attr']['loadedPrev'] = true;
			    	loaded = true;
			    }
			    if(!loaded){
				    var endpoint = isEndpoint(node);
				    if(endpoint && isAfterRoot(node)){
				    	loadNext(node, true);
				    }
				    else if(endpoint){
						loadPrevious(node, true);
				    }
			    }
			});
		}
		
		function isEndpoint(node){
			return node['attr']['endpoint'];
		}
		
		function isAfterRoot(node){
			return node['x'] > rootNode['x'];
		}
		
		function isFirstNode(node){
			var first = true;
		    sigInst.iterEdges(function(e){
		        if(e.target == node['id'])
		        	first = false;
		    });
		    return first;
		}
		
		function isLastNode(node){
			var last = true;
		    sigInst.iterEdges(function(e){
		        if(e.source == node['id'])
		        	last = false;
		    });
		    return last;
		}

		function loadLogPath(eventId, databaseId) {
			$.ajax({
				'type' : "get",
				'url' : "logviewer/ajax/logpath/new",
				'data' : {
					'eventId' : eventId,
					'databaseId' : databaseId,
				}
			}).error(function(jqXHR, textStatus, errorThrown) {
				var error = "ERROR: " + errorThrown;
				console.log(error);
				// TODO
			}).success(function(data) {
				console.log(data);
				if (data['status'] == 'ERROR') {

				} else {
					path = data['result'];
					drawPath(path);
				}
			});
		}
		
		function loadNext(node, proposals){
			$.ajax({
				'type' : "get",
				'url' : "logviewer/ajax/logpath/next",
				'data' : {
					'eventId' : node['attr']['logEvent']['graphID'],
					'databaseUrl' : node['attr']['dbUrl'],
					'proposals': proposals,
				}
			}).error(function(jqXHR, textStatus, errorThrown) {
				var error = "ERROR: " + errorThrown;
				console.log(error, jqXHR, textStatus);
				// TODO
			}).success(function(data) {
				console.log(data);
				if (data['status'] == 'ERROR') {

				} else {
					path = data['result'];
					drawNext(path, node['attr']['minY'], node['attr']['maxY']);
					rescale();
					sigInst.draw();
				}
			});
		}
		
		function loadPrevious(node, proposals){
			$.ajax({
				'type' : "get",
				'url' : "logviewer/ajax/logpath/previous",
				'data' : {
					'eventId' : node['attr']['logEvent']['graphID'],
					'databaseUrl' : node['attr']['dbUrl'],
					'proposals': proposals,
				}
			}).error(function(jqXHR, textStatus, errorThrown) {
				var error = "ERROR: " + errorThrown;
				console.log(error);
				// TODO
			}).success(function(data) {
				console.log(data);
				if (data['status'] == 'ERROR') {

				} else {
					path = data['result'];
					drawPrevious(path, node['attr']['minY'], node['attr']['maxY']);
					rescale();
					sigInst.draw();
				}
			});
		}
		
		function addNode(eventNode){
			var event = eventNode['logEvent'];
			if(sigInst._core.graph.nodesIndex[event['graphID']]){
				return;
			}
			sigInst.addNode(event['graphID'], {
				color : logColors[event['level']],
				x : event['timestamp'],
				label: '',
				logEvent: event,
				dbUrl: eventNode['eventDbUrl'],
				proposal: eventNode['proposal'],
				endpoint: eventNode['type'] == 'IN_ENDPOINT' || eventNode['type'] == 'OUT_ENDPOINT',
				drawBorder: eventNode['type'] == 'IN_ENDPOINT' || eventNode['type'] == 'OUT_ENDPOINT',
				borderColor: borderColors[eventNode['type']],
			});
		}
		
		function addEdge(source, dest){
			var sourceNode = sigInst._core.graph.nodesIndex[source];
			var destNode = sigInst._core.graph.nodesIndex[dest];
			var proposal = sourceNode['attr']['proposal'] ||
							destNode['attr']['proposal'];
			var id = source + '_' + dest;
			if(sigInst._core.graph.edgesIndex[id]){
				return;
			}
			sigInst.addEdge(
				source + '_' + dest,
				source,
				dest,
				{
					color: proposal ? proposalEdgeColor : edgeColor,
				}
			);
		}
		
		function drawPath(path) {

			var height = $('.log_path_dialog').parent().height()
					- $('.log_path_dialog').height();
			$('#log_path_graph').height(height);
			var logEvent = path['logEvent'];
			
			minTimestamp = logEvent['timestamp'];
			maxTimestamp = logEvent['timestamp'];
			
			init();
			sigInst.addNode(logEvent['graphID'], {
				color : logColors[logEvent['level']],
				x : logEvent['timestamp'],
				size : 1.5,
				label: '',
				logEvent: logEvent,
				dbUrl: path['eventDbUrl'],
				drawBorder: true,
				borderColor: edgeColor,
				proposal: false,
			});
			sigInst.iterNodes(function(n){
		        rootNode = n;
		    },[logEvent['graphID']]);
			drawNext(path);
			drawPrevious(path);
			rescale();
			sigInst.draw();
		}
		
		function drawNext(eventNode) {
			var next = eventNode['next'];
			var event = eventNode['logEvent'];
			if (!next)
				return;
			for ( var i = 0; i < next.length; i++) {
				var nextEvent = next[i]['logEvent'];
				if(nextEvent['timestamp'] > maxTimestamp)
					maxTimestamp = nextEvent['timestamp'];
				addNode(next[i]);
				addEdge(event['graphID'], nextEvent['graphID']);
				drawNext(next[i]);
			}
		}

		function drawPrevious(eventNode) {
			var previous = eventNode['previous'];
			var event = eventNode['logEvent'];
			if (!previous)
				return;
			for ( var i = 0; i < previous.length; i++) {
				var previousEvent = previous[i]['logEvent'];
				if(previousEvent['timestamp'] < minTimestamp)
					minTimestamp = previousEvent['timestamp'];
				addNode(previous[i]);
				addEdge(previousEvent['graphID'], event['graphID']);
				drawPrevious(previous[i]);
			}
		}
		
		function rescale(){
			function nextGen(node){
				var nextIds = [];
				sigInst.iterEdges(function(e){
			        if(e.source == node['id'])
			        	nextIds.push(e.target);
			    });
				var nexts = [];
				sigInst.iterNodes(function(n){
					nexts.push(n);
			    },nextIds);
				return nexts;
			}
			function prevGen(node){
				var prevIds = [];
				sigInst.iterEdges(function(e){
			        if(e.target == node['id'])
			        	prevIds.push(e.source);
			    });
				var prevs = [];
				sigInst.iterNodes(function(n){
					prevs.push(n);
			    },prevIds);
				return prevs;
			}
			function setY(node, minY, maxY, nodeGen){
				sigInst.iterNodes(function(n){
					n['y'] = minY + (maxY - minY) / 2;
			    },[node['id']]);
				var nodes = nodeGen(node);
				var slot = (maxY - minY) / nodes.length;				
				for ( var i = 0; i < nodes.length; i++) {
					setY(nodes[i], minY + slot*i, minY + slot*(i+1), nodeGen);
				}
			}
			var width = $('#log_path_graph').width();
			var height = $('#log_path_graph').height();
			var pixelSize = (maxTimestamp - minTimestamp) / width;

			var minY = 0;
			var maxY = height * pixelSize;
			setY(rootNode, minY, maxY, prevGen);
			setY(rootNode, minY, maxY, nextGen);
		}
		
		function logEventToStirng(logEvent){
			return "<ul>"+
				"<li><b>Level:</b> " + logEvent['level'] + "</li>" +
				"<li><b>Time:</b> " + logEvent['datetime'] + "</li>" +
				"<li><b>Logger:</b> " + logEvent['loggerName'] + "</li>" +
				"<li><b>Thread:</b> " + logEvent['threadName'] + "</li>" +
				"<li><b>Message:</b><br/> " + logEvent['message'] + "</li>" +
			"</ul>";
		}
		
		function showNodeInfo(event) {
			popUp && popUp.remove();

			var node;
			sigInst.iterNodes(function(n) {
				node = n;
			}, [ event.content[0] ]);
			popUp = $('<div class="node-info-popup"></div>').append(
					logEventToStirng(node['attr']['logEvent']))
						.attr('id', 'node-info' + sigInst.getID())
						.css({
							'display' : 'inline-block',
							'border-radius' : 3,
							'padding' : 5,
							'background' : '#fff',
							'color' : '#000',
							'box-shadow' : '0 0 4px #666',
							'position' : 'absolute',
							'left' : node.displayX + 10,
							'top' : node.displayY + 85,
						});

			$('#log_path_graph').append(popUp);
		}

		function hideNodeInfo(event) {
			popUp && popUp.remove();
			popUp = false;
		}

		$(function() {
			$('.logPath').click(function() {
				var eventId = $(this).attr('event');
				var databaseId = $(this).attr('database');
				openLogPath(eventId, databaseId);
			});
		});
	</script>
	
	<div class="modal_dialog log_path_dialog">
		<h3>Log Event Path</h3>
		<div id="log_path_graph" class="sig">
			
		</div>
<!-- 		<table class="details"> -->
<!-- 			<tr> -->
<!-- 				<td class="level">LEVEL</td> -->
<!-- 				<td class="datetime">123.31.312</td> -->
<!-- 				<td class="loggerName">loggerName</td> -->
<!-- 			</tr> -->
<!-- 			<tr> -->
<!-- 				<td colspan="4" class="message">Message</td> -->
<!-- 			</tr> -->
<!-- 		</table> -->
	</div>
		
	<div class="header">
		<h4>Log Events</h4>
	</div>
	
	<div id="logviewer_events" class="content">
		<c:forEach var="item" items="${neoLogEventRows}">
			<table class="log_event">
				<tr>
					<td class="level ${item.neoLogEvent.level}">${item.neoLogEvent.level}</td>
					<td class="datetime">
						<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss" value="${item.neoLogEvent.datetime}"/>
					</td>
					<td class="loggerName">${item.neoLogEvent.loggerName}</td>
					<td class="logPath" event="${item.neoLogEvent.graphID}" database="${item.databaseNode.graphID}">
<%-- 						<div class="logPathButton" event="${item.neoLogEvent.graphID}" database="${item.databaseNode.graphID}"></div> --%>
					</td>
				</tr>
				<tr>
					<td colspan="4" class="message">
						${item.neoLogEvent.message}
					</td>
				</tr>
			</table>
		</c:forEach>
	</div>
</div>
</c:if>
<jsp:include page="footer.jsp" />
