package pl.edu.agh.neolog4j.console.utils.graph;

import it.uniroma1.dis.wsngroup.gexf4j.core.Gexf;

import java.util.Set;

import pl.edu.agh.neolog4j.console.model.DatabaseNode;

public interface GraphBuilder {
	Gexf create(Set<DatabaseNode> databases);
}
