package pl.edu.agh.neolog4j.console.dao;

import java.util.Set;

import org.neo4j.graphdb.Transaction;
import org.springframework.stereotype.Service;

import pl.edu.agh.neolog4j.console.model.DatabaseExistException;
import pl.edu.agh.neolog4j.console.model.DatabaseNode;
import pl.edu.agh.neolog4j.dao.AbstractDao;
import pl.edu.agh.neolog4j.model.ApplicationInstance;

@Service
public class DatabaseNodeDao extends AbstractDao {

	public Set<DatabaseNode> getDatabases() {
		return getAllNodes(DatabaseNode.class);
	}

	public DatabaseNode addDatabase(String url) throws DatabaseExistException {
		DatabaseNode dataBase = this.template.repositoryFor(DatabaseNode.class).findByPropertyValue("url", url);
		if (dataBase != null) {
			throw new DatabaseExistException();
		}
		return getDatabase(url);
	}


	/** 
	 * Node will be created if not existing
	 */
	public DatabaseNode getDatabase(String url) {
		return getOrCreateNamedNode(url, DatabaseNode.DATABASE_NODE_INDEX_NAME, DatabaseNode.class);
	}
	
	public DatabaseNode get(long graphId){
		return getTemplate().findOne(graphId, DatabaseNode.class);
	}

	public void deleteDatabase(DatabaseNode database) {
		Transaction tx = this.template.getGraphDatabase().beginTx();
		try {
			this.template.delete(database);
			tx.success();
		} finally {
			tx.finish();
		}
	}

	public void addApplicationInstance(DatabaseNode p_database,
			ApplicationInstance p_instance) {
		Transaction tx = this.template.getGraphDatabase().beginTx();
		try {
			DatabaseNode database = requery(p_database, DatabaseNode.class);
			database.getApplicationInstances().add(p_instance);
			p_database.setApplicationInstances(database.getApplicationInstances());
			tx.success();
		} finally {
			tx.finish();
		}
		
	}

}
