package pl.edu.agh.neolog4j.console.form;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import pl.edu.agh.neolog4j.dao.query.LogEventField;
import pl.edu.agh.neolog4j.dao.query.LogLevel;
import pl.edu.agh.neolog4j.dao.query.OrderType;

public class LogEventFilter {
	private Set<LogLevel> levels = new HashSet<LogLevel>();
	private Set<String> applications = new HashSet<String>(); 
	private Date after;
	private Date before;
	private LogEventField orderBy = LogEventField.DATE;
	private OrderType orderType = OrderType.ASC;
	private int offset = 0;
	private int limit = 0;
	
	public Set<LogLevel> getLevels() {
		return levels;
	}
	public void setLevels(Set<LogLevel> levels) {
		this.levels = levels;
	}
	public Set<String> getApplications() {
		return applications;
	}
	public void setApplications(Set<String> applications) {
		this.applications = applications;
	}
	public Date getAfter() {
		return after;
	}
	public void setAfter(Date after) {
		this.after = after;
	}
	public Date getBefore() {
		return before;
	}
	public void setBefore(Date before) {
		this.before = before;
	}
	public LogEventField getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(LogEventField orderBy) {
		this.orderBy = orderBy;
	}
	public OrderType getOrderType() {
		return orderType;
	}
	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	
}
