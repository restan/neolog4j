package pl.edu.agh.neolog4j.console.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;

import pl.edu.agh.neolog4j.model.AbstractModelClass;
import pl.edu.agh.neolog4j.model.ApplicationInstance;
import pl.edu.agh.neolog4j.model.UniqueNamedNodesInterface;
import pl.edu.agh.neolog4j.utils.json.JsonDateSerializer;

@NodeEntity
public class DatabaseNode extends AbstractModelClass implements Comparable<DatabaseNode>, UniqueNamedNodesInterface {
	
	public final static String DATABASE_NODE_INDEX_NAME = "url";
	
	@Indexed(indexName = DATABASE_NODE_INDEX_NAME, unique = true)
	private String url;
	private Date lastSynchronization;
	private Set<ApplicationInstance> applicationInstances = new HashSet<ApplicationInstance>();

	@JsonIgnore
	public Set<ApplicationInstance> getApplicationInstances() {
		return applicationInstances;
	}
	
	public void setApplicationInstances(Set<ApplicationInstance> applicationInstances) {
		this.applicationInstances = applicationInstances;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	@JsonSerialize(using=JsonDateSerializer.class)
	public Date getLastSynchronization() {
		return lastSynchronization;
	}

	public void setLastSynchronization(Date lastSynchronization) {
		this.lastSynchronization = lastSynchronization;
	}

	@Override
	public int compareTo(DatabaseNode o) {
		return this.getUrl().compareTo(o.getUrl());
	}

	@Override
	public String getName() {
		return this.url;
	}

	@Override
	public void setName(String p_name) {
		this.url = p_name;	
	}	
}
