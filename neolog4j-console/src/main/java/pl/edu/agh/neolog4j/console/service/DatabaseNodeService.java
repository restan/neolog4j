package pl.edu.agh.neolog4j.console.service;

import java.util.Date;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.edu.agh.neolog4j.console.dao.DatabaseNodeDao;
import pl.edu.agh.neolog4j.console.model.DatabaseConnectionError;
import pl.edu.agh.neolog4j.console.model.DatabaseExistException;
import pl.edu.agh.neolog4j.console.model.DatabaseNode;
import pl.edu.agh.neolog4j.dao.ApplicationDao;
import pl.edu.agh.neolog4j.dao.DaoFactory;
import pl.edu.agh.neolog4j.dao.EndpointDao;
import pl.edu.agh.neolog4j.model.Application;
import pl.edu.agh.neolog4j.model.ApplicationInstance;
import pl.edu.agh.neolog4j.model.Endpoint;
import pl.edu.agh.neolog4j.model.InEndpoint;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

@Service
public class DatabaseNodeService {
	
	private static int SERVER_OK_STATUS = 200;
	
	@Autowired
	private DatabaseNodeDao databaseNodeDao;
	@Autowired
	private ApplicationDao applicationDao;
	@Autowired
	private EndpointDao endpointDao;
	
		
	public DatabaseNode addDatabase(String url, boolean testConnection) throws DatabaseConnectionError, DatabaseExistException{
		if(testConnection && !testConnection(url)){
			throw new DatabaseConnectionError();
		}
		DatabaseNode database = this.databaseNodeDao.addDatabase(url);
		return database;
	}
	
	protected void synchroniseDatabase(DaoFactory remoteDb, DatabaseNode database) {
		Set<Application> remoteApplications = remoteDb.getApplicationDao().getAllNodes(Application.class);
		for (Application remoteApplication: remoteApplications) {
			Set<ApplicationInstance> remoteInstances = remoteDb
					.getApplicationDao().getInstances(
							remoteApplication.getName(),
							database.getLastSynchronization());
			for (ApplicationInstance remoteInstance: remoteInstances) {
				ApplicationInstance instance = this.applicationDao
						.getApplicationInstance(remoteApplication.getName(),
								remoteInstance.getCreateDate());
				this.databaseNodeDao.addApplicationInstance(database, instance);
			}
			for (Endpoint remoteEndpoint: remoteDb.getApplicationDao().getApplicationEndpoints(remoteApplication)) {
				if (remoteEndpoint instanceof InEndpoint) {
					this.endpointDao
							.getInEndpoint(remoteEndpoint.getName(),
									remoteApplication.getName());
				} else {
					this.endpointDao
							.getOutEndpoint(remoteEndpoint.getName(),
									remoteApplication.getName());
				}
			}
		}
		database.setLastSynchronization(new Date());
		databaseNodeDao.getTemplate().save(database);
	}
	
	public void synchroniseDatabase(DatabaseNode database) throws DatabaseConnectionError{
		DaoFactory remoteDb = DaoFactory.getInstance(database.getUrl());
		synchroniseDatabase(remoteDb, database);
	}
	
	public Set<DatabaseNode> getDatabases(){
		// zwraca wszystkie bazy z bazy
		return this.databaseNodeDao.getDatabases();
	}
	
	public boolean testConnection(String url){
		
		WebResource resource = Client.create()
		        .resource(url);
		ClientResponse response = null;
		try {
			response = resource.get(ClientResponse.class);
		} catch (ClientHandlerException e) {
			return false;
		}
		return response.getStatus() == SERVER_OK_STATUS;

	}

	public DatabaseNode getDatabase(String url) {
		// zwraca baz� na podstawie url
		return this.databaseNodeDao.getDatabase(url);
	}

	public DatabaseNodeDao getDatabaseNodeDao() {
		return databaseNodeDao;
	}

	public void setDatabaseNodeDao(DatabaseNodeDao databaseNodeDao) {
		this.databaseNodeDao = databaseNodeDao;
	}

	public void deleteDatabase(DatabaseNode database) {
		// usuwa baz�
		this.databaseNodeDao.deleteDatabase(database);
	}
}
