package pl.edu.agh.neolog4j.console.utils.graph;

import it.uniroma1.dis.wsngroup.gexf4j.core.EdgeType;
import it.uniroma1.dis.wsngroup.gexf4j.core.Gexf;
import it.uniroma1.dis.wsngroup.gexf4j.core.Graph;
import it.uniroma1.dis.wsngroup.gexf4j.core.Mode;
import it.uniroma1.dis.wsngroup.gexf4j.core.Node;
import it.uniroma1.dis.wsngroup.gexf4j.core.data.Attribute;
import it.uniroma1.dis.wsngroup.gexf4j.core.data.AttributeClass;
import it.uniroma1.dis.wsngroup.gexf4j.core.data.AttributeList;
import it.uniroma1.dis.wsngroup.gexf4j.core.data.AttributeType;
import it.uniroma1.dis.wsngroup.gexf4j.core.impl.GexfImpl;
import it.uniroma1.dis.wsngroup.gexf4j.core.impl.data.AttributeListImpl;
import it.uniroma1.dis.wsngroup.gexf4j.core.impl.viz.ColorImpl;
import it.uniroma1.dis.wsngroup.gexf4j.core.viz.Color;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import pl.edu.agh.neolog4j.console.model.DatabaseNode;
import pl.edu.agh.neolog4j.dao.DaoFactory;
import pl.edu.agh.neolog4j.model.Application;
import pl.edu.agh.neolog4j.model.ApplicationInstance;
import pl.edu.agh.neolog4j.model.Endpoint;
import pl.edu.agh.neolog4j.model.InEndpoint;
import pl.edu.agh.neolog4j.model.OutEndpoint;

public class ConfigurationGraphBuilder implements GraphBuilder {
	private static final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final Color DATABASE_NODE_COLOR = new ColorImpl(8, 230, 0);
	private static final Color APPLICATION_INSTANCE_COLOR = new ColorImpl(230, 130, 0);
	private static final Color APPLICATION_COLOR = new ColorImpl(204, 201, 0);
	private static final Color IN_ENDPOINT_COLOR = new ColorImpl(0, 80, 145);
	private static final Color OUT_ENDPOINT_COLOR = new ColorImpl(133, 224, 255);
	
	private static final String ATTR_NODE_TYPE = "ATTR_NODE_TYPE";
	private static final String ATTR_DB_URL = "ATTR_DB_URL";
	private static final String ATTR_DB_LAST_CONNECTION = "ATTR_DB_LAST_CONNECTION";
	private static final String ATTR_APP_NAME = "ATTR_APP_NAME";
	private static final String ATTR_EP_NAME = "ATTR_EP_NAME";
	private static final String ATTR_EP_APPNAME = "ATTR_EP_APPNAME";
	
	private DaoFactory daoFactory;
	private Map<String, Attribute> attributes = new HashMap<String, Attribute>();
	private Graph graph;
	private Gexf gexf;
	private Map<Application, Node> appNodes = new HashMap<Application, Node>();
	private Map<InEndpoint, Node> inEpNodes = new HashMap<InEndpoint, Node>();
	
	public ConfigurationGraphBuilder(DaoFactory daoFactory){
		this.daoFactory = daoFactory;
	}
	
	public Gexf create(Set<DatabaseNode> databases){
		initGraph();
		initAttributes();
		for(DatabaseNode database : databases){
			addDatabase(database);
		}
		return gexf;
	}
	
	private void addDatabase(DatabaseNode database){
		Node databaseNode = graph.createNode();
		databaseNode.setColor(DATABASE_NODE_COLOR);
		databaseNode.setLabel(database.getUrl())
			.getAttributeValues()
				.addValue(attributes.get(ATTR_NODE_TYPE), "database")
				.addValue(attributes.get(ATTR_DB_URL), database.getUrl())
				.addValue(
					attributes.get(ATTR_DB_LAST_CONNECTION),
					database.getLastSynchronization() != null ?
						df.format(database.getLastSynchronization()) : "---"
				);
		for(ApplicationInstance appIns : database.getApplicationInstances()){
			Node appInsNode = addApplicationInstanca(appIns);
			databaseNode.connectTo(appInsNode).setEdgeType(EdgeType.DIRECTED);
		}
	}
	
	private Node addApplicationInstanca(ApplicationInstance appIns){
		appIns = daoFactory.getApplicationDao().requery(appIns, ApplicationInstance.class);
		Node appInsNode = graph.createNode();
		appInsNode.setColor(APPLICATION_INSTANCE_COLOR);
		appInsNode.setLabel(df.format(appIns.getCreateDate()));
		Application application = appIns.getApplication();
		Node appNode = appNodes.get(application);
		if(appNode == null){
			appNode = addApplication(application);
		}
		appInsNode.connectTo(appNode);
		return appInsNode;
	}

	private Node addApplication(Application application) {
		Node appNode = graph.createNode();
		appNode.setColor(APPLICATION_COLOR);
		appNodes.put(application, appNode);
		String name = application.getName();
		appNode.setLabel(name)
			.getAttributeValues()
				.addValue(attributes.get(ATTR_NODE_TYPE), "application")
				.addValue(attributes.get(ATTR_APP_NAME), name);
		for(Endpoint endpoint: daoFactory.getApplicationDao().getApplicationEndpoints(application)){
			Node epNode = addEndpoint(endpoint);
			appNode.connectTo(epNode).setEdgeType(EdgeType.DIRECTED);
		}
		return appNode;
	}

	private Node addEndpoint(Endpoint endpoint) {
		if(endpoint instanceof InEndpoint){
			InEndpoint inEndpoint = (InEndpoint) endpoint;
			return addInEndpoint(inEndpoint);
		}
		else if(endpoint instanceof OutEndpoint){
			OutEndpoint outEndpoint = (OutEndpoint) endpoint;
			return addOutEndpoint(outEndpoint);
		}
		else{
			return null;
		}
	}

	private Node addOutEndpoint(OutEndpoint outEndpoint) {
		outEndpoint = daoFactory.getEndpointDao().requery(outEndpoint, OutEndpoint.class);
		Application application = outEndpoint.getApplication();
		Node outEpNode = graph.createNode();
		String name = outEndpoint.getName();
		outEpNode.setLabel(name)
			.setColor(OUT_ENDPOINT_COLOR)
			.getAttributeValues()
				.addValue(attributes.get(ATTR_NODE_TYPE), "outEndpoint")
				.addValue(attributes.get(ATTR_EP_NAME), name)
				.addValue(attributes.get(ATTR_EP_APPNAME), application.getName());
		for(InEndpoint inEndpoint : outEndpoint.getDestinations()){
			Node inEpNode = addInEndpoint(inEndpoint);
			outEpNode.connectTo(inEpNode).setEdgeType(EdgeType.DIRECTED);
		}
		return outEpNode;
	}

	private Node addInEndpoint(InEndpoint inEndpoint) {
		inEndpoint = daoFactory.getEndpointDao().requery(inEndpoint, InEndpoint.class);
		Application application = inEndpoint.getApplication();
		Node inEpNode = inEpNodes.get(inEndpoint);
		if(inEpNode == null){
			inEpNode = graph.createNode();
			String name = inEndpoint.getName();
			inEpNode.setLabel(name)
				.setColor(IN_ENDPOINT_COLOR)
				.getAttributeValues()
					.addValue(attributes.get(ATTR_NODE_TYPE), "inEndpoint")
					.addValue(attributes.get(ATTR_EP_NAME), name)
					.addValue(attributes.get(ATTR_EP_APPNAME), application.getName());
			inEpNodes.put(inEndpoint, inEpNode);
		}
		return inEpNode;
	}

	private void initGraph(){
		Gexf gexf = new GexfImpl();
		gexf.setVisualization(true);
		Calendar date = Calendar.getInstance();
		gexf.getMetadata()
			.setLastModified(date.getTime())
			.setCreator("NeoLog4j")
			.setDescription("Configuration graph");
				
		Graph graph = gexf.getGraph();
		graph.setDefaultEdgeType(EdgeType.UNDIRECTED).setMode(Mode.STATIC);
		
		this.graph = graph;
		this.gexf = gexf;
	}
	
	private void initAttributes(){
		AttributeList nodeAttrs = new AttributeListImpl(AttributeClass.NODE);
		graph.getAttributeLists().add(nodeAttrs);
		
		Attribute typeAttr = nodeAttrs.createAttribute("type", AttributeType.STRING, "type");
		
		AttributeList databaseAttrs = new AttributeListImpl(AttributeClass.NODE);
		graph.getAttributeLists().add(databaseAttrs);
		
		Attribute dbAttUrl = databaseAttrs.createAttribute("url", AttributeType.STRING, "url");
		Attribute dbAttrLastConnected = databaseAttrs.createAttribute("lastConnected", AttributeType.STRING, "lastConnected");

		AttributeList applicationInstanceAttrs = new AttributeListImpl(AttributeClass.NODE);
		graph.getAttributeLists().add(applicationInstanceAttrs);

		AttributeList applicationAttrs = new AttributeListImpl(AttributeClass.NODE);
		graph.getAttributeLists().add(applicationAttrs);

		Attribute appAttName = applicationAttrs.createAttribute("name", AttributeType.STRING, "name");

		AttributeList endpointAttrs = new AttributeListImpl(AttributeClass.NODE);
		graph.getAttributeLists().add(endpointAttrs);
		
		Attribute epAttName = endpointAttrs.createAttribute("name", AttributeType.STRING, "name");
		Attribute epAttAppName = endpointAttrs.createAttribute("appName", AttributeType.STRING, "appName");
		
		attributes.put(ATTR_NODE_TYPE, typeAttr);
		attributes.put(ATTR_DB_URL, dbAttUrl);
		attributes.put(ATTR_DB_LAST_CONNECTION, dbAttrLastConnected);
		attributes.put(ATTR_APP_NAME, appAttName);
		attributes.put(ATTR_EP_NAME, epAttName);
		attributes.put(ATTR_EP_APPNAME, epAttAppName);
	}
}
