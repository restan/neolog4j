package pl.edu.agh.neolog4j.console.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.conversion.EndResult;
import org.springframework.data.neo4j.conversion.Result;
import org.springframework.stereotype.Service;

import pl.edu.agh.neolog4j.console.model.DatabaseNode;
import pl.edu.agh.neolog4j.console.model.LogPathNode;
import pl.edu.agh.neolog4j.console.model.LogPathNodeType;
import pl.edu.agh.neolog4j.console.model.NeoLogEventRow;
import pl.edu.agh.neolog4j.console.model.NeoLogEventRowComparator;
import pl.edu.agh.neolog4j.dao.AbstractDao;
import pl.edu.agh.neolog4j.dao.ApplicationDao;
import pl.edu.agh.neolog4j.dao.DaoFactory;
import pl.edu.agh.neolog4j.dao.EndpointDao;
import pl.edu.agh.neolog4j.dao.query.LogEventField;
import pl.edu.agh.neolog4j.dao.query.LogEventQuery;
import pl.edu.agh.neolog4j.dao.query.OrderType;
import pl.edu.agh.neolog4j.model.ApplicationInstance;
import pl.edu.agh.neolog4j.model.InEndpoint;
import pl.edu.agh.neolog4j.model.NeoLogEvent;
import pl.edu.agh.neolog4j.model.NeoLogEventComparator;
import pl.edu.agh.neolog4j.model.OutEndpoint;

@Service
public class LogEventService extends AbstractDao {
	
	private static final int PROPOSAL_LIMIT = 10;
	private static final int PROPOSAL_TIME_OFFSET = 10;
	
	@Autowired
	protected ApplicationDao applicationDao;
	
	@Autowired
	protected EndpointDao endpointDao;
	
	
	public List<NeoLogEventRow> getByQuery(LogEventQuery query){
		List<NeoLogEventRow> result = new ArrayList<NeoLogEventRow>();
		Map<DatabaseNode, LogEventQuery> queriesForBase = getQueriesForBase(query);
		for (DatabaseNode database: queriesForBase.keySet()) {
			DaoFactory remoteDb = DaoFactory.getInstance(database.getUrl());
			result.addAll(getRows(remoteDb.getNeoLogEventDao().query(queriesForBase.get(database)), database));
		}
		NeoLogEventRowComparator comparator = new NeoLogEventRowComparator(query.getOrderBy());
		Collections.sort(result, comparator);
		if (query.getOrderType() == OrderType.DESC) {
			Collections.reverse(result);
		}
		if (!result.isEmpty() && query.getLimit() > 0) {
			result = new ArrayList<NeoLogEventRow>(result.subList(0, Math.min(query.getLimit(), result.size())));
		}
		return result;
	}
	
	protected Map<DatabaseNode, LogEventQuery> getQueriesForBase(LogEventQuery p_query) {
		Map<DatabaseNode, LogEventQuery> result = new HashMap<DatabaseNode, LogEventQuery>();
		Map<DatabaseNode, List<ApplicationInstance>> instacesPerBase = getInstancesPerBase(p_query);
		for (DatabaseNode database: instacesPerBase.keySet()) {
			LogEventQuery remoteQuery = getRemoteQuery(p_query);
			result.put(database, remoteQuery);
			List<ApplicationInstance> instances = instacesPerBase.get(database);
			Map<String, Set<Long>> instancesPerApplication = instancesTimestampsPerApplication(instances);
			DaoFactory remoteDb = DaoFactory.getInstance(database.getUrl());
			for (String appName: instancesPerApplication.keySet()) {
				Set<ApplicationInstance> remoteInstances = remoteDb
						.getApplicationDao().getApplicationInstances(appName,
								instancesPerApplication.get(appName));
				remoteQuery.getApplicationInstances().addAll(remoteInstances);
			}	
		}
		return result;
	}
	
	protected Map<String, Set<Long>> instancesTimestampsPerApplication(List<ApplicationInstance> p_instances) {
		
		Map<String, Set<Long>> result = new HashMap<String, Set<Long>>();
		for (ApplicationInstance instance: p_instances) {
			String appName = instance.getApplication().getName();
			if (!result.containsKey(appName)) {
				Set<Long> instancesList = new HashSet<Long>();
				result.put(appName, instancesList);
			}
			result.get(appName).add(instance.getCreateTimestamp());
		}
		return result;
	}
	
	protected LogEventQuery getRemoteQuery(LogEventQuery p_localQuery) {
		LogEventQuery remoteQuery = new LogEventQuery();
		remoteQuery.setAfter(p_localQuery.getAfter());
		remoteQuery.setBefore(p_localQuery.getBefore());
		remoteQuery.setLevels(p_localQuery.getLevels());
		remoteQuery.setLimit(p_localQuery.getLimit());
		remoteQuery.setOffset(p_localQuery.getOffset());
		remoteQuery.setOrderBy(p_localQuery.getOrderBy());
		remoteQuery.setOrderType(p_localQuery.getOrderType());
		remoteQuery.setOutEndpoints(p_localQuery.getOutEndpoints());
		remoteQuery.setInEndpoints(p_localQuery.getInEndpoints());
		return remoteQuery;
		
	}
	
	protected Map<DatabaseNode, List<ApplicationInstance>> getInstancesPerBase(LogEventQuery p_query) {
		Map<DatabaseNode, List<ApplicationInstance>> instacesPerBase = new HashMap<DatabaseNode, List<ApplicationInstance>>();
		for (ApplicationInstance instance: p_query.getApplicationInstances()) {
			DatabaseNode databaseNode = getDatabaseNode(instance);
			if (!instacesPerBase.containsKey(databaseNode)) {
				List<ApplicationInstance> instances = new ArrayList<ApplicationInstance>();
				instacesPerBase.put(databaseNode, instances);
			}
			instacesPerBase.get(databaseNode).add(instance);
		}
		return instacesPerBase;
	}
	
	protected DatabaseNode getDatabaseNode(ApplicationInstance p_instance) {
		String query = "start instance=node({instanceID})" +
				"match instance <-[:applicationInstances]-(database)" +
				"return database";
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("instanceID", p_instance.getGraphID());
		Result<Map<String,Object>> queryResult = this.template.query(query, parameters);
		EndResult<DatabaseNode> endResult = queryResult.to(DatabaseNode.class);
		return iterableToSet(endResult).iterator().next();		
	}
	
	protected List<NeoLogEventRow> getRows(Collection<NeoLogEvent> events, DatabaseNode databaseNode){
		List<NeoLogEventRow> rows = new ArrayList<NeoLogEventRow>();
		for(NeoLogEvent event: events){
			NeoLogEventRow row = new NeoLogEventRow();
			row.setDatabaseNode(databaseNode);
			row.setNeoLogEvent(event);
			rows.add(row);
		}
		return rows;
	}
	
	public LogPathNode getNewLogPath(long p_eventId, DatabaseNode p_database){
		return getNewLogPath(p_eventId, p_database, 4, 4, false);
	}
	
	public LogPathNode getNewLogPath(long p_eventId, DatabaseNode p_database, int lenBefore, int lenAfter, boolean proposals){
		return getNewLogPath(p_eventId, p_database.getUrl(), lenBefore, lenAfter, proposals);
	}
	
	public LogPathNode getNewLogPath(long p_eventId, String p_databaseUrl, int lenBefore, int lenAfter, boolean proposals){
		
		DaoFactory remoteDb = DaoFactory.getInstance(p_databaseUrl);
		NeoLogEvent event = remoteDb.getNeoLogEventDao().getTemplate().findOne(p_eventId, NeoLogEvent.class);
		LogPathNode result = getNormalPathNode(event, remoteDb);
		if(!proposals){
			loadNext(result, remoteDb, lenAfter);
			loadPrevious(result, remoteDb, lenBefore);
		}
		else{
			loadNextProposals(result, remoteDb, lenAfter);
			loadPreviousProposals(result, remoteDb, lenBefore);
		}
		return result;
	}
	
	protected LogEventQuery getNextProposalQuery(LogPathNode p_node) {
		LogEventQuery query = new LogEventQuery();
		query.setLimit(PROPOSAL_LIMIT);
		Calendar before = Calendar.getInstance();
		before.setTime(p_node.getLogEvent().getDatetime());
		before.add(Calendar.SECOND, PROPOSAL_TIME_OFFSET);
		Calendar after = Calendar.getInstance();
		after.setTime(p_node.getLogEvent().getDatetime());
		query.setAfter(after.getTime());
		query.setBefore(before.getTime());
		return query;
	}
	
	protected LogEventQuery getPrevProposalQuery(LogPathNode p_node) {
		LogEventQuery query = new LogEventQuery();
		query.setLimit(PROPOSAL_LIMIT);
		Calendar before = Calendar.getInstance();
		before.setTime(p_node.getLogEvent().getDatetime());
		Calendar after = Calendar.getInstance();
		after.setTime(p_node.getLogEvent().getDatetime());
		after.add(Calendar.SECOND, -PROPOSAL_TIME_OFFSET);
		query.setAfter(after.getTime());
		query.setBefore(before.getTime());
		return query;
	}
	
	protected void loadNextProposals(LogPathNode p_node, DaoFactory p_remoteDb, int p_lenAfter) {
		NeoLogEvent logEvent = p_node.getLogEvent();
		OutEndpoint remoteOutEndpoint = logEvent.getDestinationEndpoint();
		if (remoteOutEndpoint != null) {
			LogEventQuery query = getNextProposalQuery(p_node);
			OutEndpoint localOutEndpoint = this.endpointDao.getOutEndpoint(remoteOutEndpoint.getName(), remoteOutEndpoint.getApplication().getName());
			Set<InEndpoint> inEndpoints = localOutEndpoint.getDestinations();
			for (InEndpoint endpoint: inEndpoints) {
				endpoint = this.endpointDao.requery(endpoint, InEndpoint.class);
				query.addApplicationInstances(this.applicationDao
						.getAllInstances(endpoint.getApplication()));
				query.addInEndpoint(endpoint.getName());
			}
			List<LogEventDatabseEntry> inLogsList = getLogEventDbEntries(query);
			Collections.sort(inLogsList);
			if (PROPOSAL_LIMIT > 0) {
				inLogsList = new ArrayList<LogEventDatabseEntry>(inLogsList.subList(0, Math.min(PROPOSAL_LIMIT, inLogsList.size())));
			}
			for (LogEventDatabseEntry logEventDbEntry: inLogsList) {
				NeoLogEvent inNextEvent = logEventDbEntry.getValue();
				DaoFactory logsDb = DaoFactory.getInstance(logEventDbEntry.getKey().getUrl());
				LogPathNode inPathNode = getInPathNode(inNextEvent, logsDb);
				p_node.addNext(inPathNode);
//				inPathNode.addPrevious(p_node);
				if (p_lenAfter > 0) {				
					loadNext(inPathNode, logsDb, p_lenAfter - 1);
				}
			}			
		}
	}
	
	protected List<LogEventDatabseEntry> getLogEventDbEntries(LogEventQuery p_query) {
		List<LogEventDatabseEntry> result = new ArrayList<LogEventDatabseEntry>();
		Map<DatabaseNode, LogEventQuery> queriesForBase = getQueriesForBase(p_query);
		for (DatabaseNode database: queriesForBase.keySet()) {
			DaoFactory logsDb = DaoFactory.getInstance(database.getUrl());
			Set<NeoLogEvent> logs = logsDb.getNeoLogEventDao().query(queriesForBase.get(database));
			for (NeoLogEvent event: logs) {
				result.add(new LogEventDatabseEntry(database, event));
			}
		}
		return result;
	}
	
	protected void loadPreviousProposals(LogPathNode p_node, DaoFactory p_remoteDb, int p_lenBefore) {
		NeoLogEvent logEvent = p_node.getLogEvent();
		InEndpoint remoteInEndpoint = logEvent.getSourceEndpoint();
		if (remoteInEndpoint != null) {
			InEndpoint localIn = this.endpointDao.getInEndpoint(remoteInEndpoint.getName(), remoteInEndpoint.getApplication().getName());
			LogEventQuery query = getPrevProposalQuery(p_node);
			Set<OutEndpoint> outEndpoints = localIn.getSources();//getRemoteSources(logEvent.getSourceEndpoint(), p_remoteDb);
			for (OutEndpoint endpoint: outEndpoints) {
				endpoint = this.endpointDao.requery(endpoint, OutEndpoint.class);
				query.addApplicationInstances(this.applicationDao
						.getAllInstances(endpoint.getApplication()));
				query.addOutEndpoint(endpoint.getName());
			}
			List<LogEventDatabseEntry> outLogsList = getLogEventDbEntries(query);
			Collections.sort(outLogsList);
			Collections.reverse(outLogsList);
			if (PROPOSAL_LIMIT > 0) {
				outLogsList = new ArrayList<LogEventDatabseEntry>(outLogsList.subList(0, Math.min(PROPOSAL_LIMIT, outLogsList.size())));
			}
			for (LogEventDatabseEntry logEventDbEntry: outLogsList) {
				NeoLogEvent outPrevEvent = logEventDbEntry.getValue();
				DaoFactory logsDb = DaoFactory.getInstance(logEventDbEntry.getKey().getUrl());
				LogPathNode outPathNode = getOutPathNode(outPrevEvent, logsDb);
				p_node.addPrevious(outPathNode);
//				outPathNode.addNext(p_node);
				if (p_lenBefore > 0) {				
					loadPrevious(outPathNode, logsDb, p_lenBefore - 1);
				}
			}
		}
	}
	
	protected void loadNext(LogPathNode p_node, DaoFactory p_remoteDb, int p_lenAfter) {
		NeoLogEvent logEvent = p_node.getLogEvent();
		Set<NeoLogEvent> nextEvents = p_remoteDb.getNeoLogEventDao()
				.getNextEvents(logEvent);
		for (NeoLogEvent nextEvent: nextEvents) {
			LogPathNode normalPathNode = getNormalPathNode(nextEvent, p_remoteDb);	
			p_node.addNext(normalPathNode);
			//normalPathNode.addPrevious(p_node);
			if (p_lenAfter > 0) {				
				loadNext(normalPathNode, p_remoteDb, p_lenAfter - 1);
			}
		}
		//loadNextProposals(p_node, p_remoteDb, p_lenAfter - 1);
	}
	
	protected void loadPrevious(LogPathNode p_node, DaoFactory p_remoteDb, int p_lenBefore) {
		NeoLogEvent logEvent = p_node.getLogEvent();
		NeoLogEvent previous = logEvent.getPrevious();
		if(previous != null){
			previous = p_remoteDb.getNeoLogEventDao().requery(previous, NeoLogEvent.class);
			LogPathNode normalPathNode = getNormalPathNode(previous, p_remoteDb);
			//normalPathNode.addNext(p_node);
			p_node.addPrevious(normalPathNode);
			if (p_lenBefore > 0) {
				loadPrevious(normalPathNode, p_remoteDb, p_lenBefore - 1);
			}
		}
		//loadPreviousProposals(p_node, p_remoteDb, p_lenBefore - 1);
	}
	
	protected LogPathNode getNormalPathNode(NeoLogEvent p_event, DaoFactory p_eventDb) {
		LogPathNode result = new LogPathNode();
		result.setProposal(false);
		if(p_event.getDestinationEndpoint() != null){
			result.setType(LogPathNodeType.OUT_ENDPOINT);
		}
		else if(p_event.getSourceEndpoint() != null){
			result.setType(LogPathNodeType.IN_ENDPOINT);
		}
		else{
			result.setType(LogPathNodeType.NORMAL);
		}
		result.setLogEvent(p_event);
		result.setEventDbUrl(p_eventDb.getUrl());
		return result;
	}
	
	protected LogPathNode getInPathNode(NeoLogEvent p_event, DaoFactory p_eventDb) {
		LogPathNode result = new LogPathNode();
		result.setProposal(true);
		result.setType(LogPathNodeType.IN_ENDPOINT);
		result.setLogEvent(p_event);
		result.setEventDbUrl(p_eventDb.getUrl());
		return result;
	}
	
	protected LogPathNode getOutPathNode(NeoLogEvent p_event, DaoFactory p_eventDb) {
		LogPathNode result = new LogPathNode();
		result.setProposal(true);
		result.setType(LogPathNodeType.OUT_ENDPOINT);
		result.setLogEvent(p_event);
		result.setEventDbUrl(p_eventDb.getUrl());
		return result;
	}
	
	public void loadNextNodes(LogPathNode node, int lenAfter) {
		DaoFactory remoteDb = DaoFactory.getInstance(node.getEventDbUrl());
		loadNext(node, remoteDb, lenAfter);
		
	}
	
	public void loadPreviousNodes(LogPathNode node, int lenBefore){
		DaoFactory remoteDb = DaoFactory.getInstance(node.getEventDbUrl());
		loadPrevious(node, remoteDb, lenBefore);
	}
	
	protected class LogEventDatabseEntry implements Comparable<LogEventDatabseEntry> {

		protected DatabaseNode databaseNode;
		protected NeoLogEvent neoLogEvent;
		
		public LogEventDatabseEntry(DatabaseNode p_key, NeoLogEvent p_val) {
			this.databaseNode = p_key;
			this.neoLogEvent = p_val;
		}
		
		public DatabaseNode getKey() {
			return this.databaseNode;
		}

		public NeoLogEvent getValue() {
			return this.neoLogEvent;
		}

		@Override
		public int compareTo(LogEventDatabseEntry o) {
			NeoLogEventComparator dateComparator = new NeoLogEventComparator(LogEventField.DATE);
			return dateComparator.compare(this.getValue(), o.getValue());
		}
		
	}
}
