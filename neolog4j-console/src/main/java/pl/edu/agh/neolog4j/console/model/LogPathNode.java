package pl.edu.agh.neolog4j.console.model;

import java.util.LinkedHashSet;
import java.util.Set;

import pl.edu.agh.neolog4j.model.NeoLogEvent;

public class LogPathNode {
	private NeoLogEvent logEvent;
	private String eventDbUrl;
	private Set<LogPathNode> next;
	private Set<LogPathNode> previous;
	private LogPathNodeType type;
	private boolean proposal;
	
	public NeoLogEvent getLogEvent() {
		return logEvent;
	}
	public void setEventDbUrl(final String p_url) {
		this.eventDbUrl = p_url;
	}
	public String getEventDbUrl() {
		return this.eventDbUrl;
	}
	
	public void setLogEvent(final NeoLogEvent logEvent) {
		this.logEvent = logEvent;
	}
	public Set<LogPathNode> getNext() {
		return next;
	}
	public void setNext(final Set<LogPathNode> next) {
		this.next = next;
	}
	public void addNext(final LogPathNode p_next) {
		if (this.next == null) {
			this.next = new LinkedHashSet<LogPathNode>();
		}
		this.next.add(p_next);
	}
	public Set<LogPathNode> getPrevious() {
		return previous;
	}
	public void addPrevious(final LogPathNode p_previous) {
		if (this.previous == null) {
			this.previous = new LinkedHashSet<LogPathNode>();
		}
		this.previous.add(p_previous);
	}
	public void setPrevious(final Set<LogPathNode> previous) {
		this.previous = previous;
	}
	public LogPathNodeType getType() {
		return type;
	}
	public void setType(final LogPathNodeType type) {
		this.type = type;
	}
	public boolean isProposal() {
		return proposal;
	}
	public void setProposal(final boolean proposal) {
		this.proposal = proposal;
	}
	
	@Override
	public boolean equals(final Object arg0) {
		if (arg0 != null && arg0 instanceof LogPathNode) {
			LogPathNode other = (LogPathNode) arg0;
			return this.logEvent.equals(other.logEvent)
					&& this.eventDbUrl.equals(other.eventDbUrl);
		}
		return false;
	}
}
