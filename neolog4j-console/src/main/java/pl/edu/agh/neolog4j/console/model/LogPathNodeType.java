package pl.edu.agh.neolog4j.console.model;

public enum LogPathNodeType {
	NORMAL,
	IN_ENDPOINT,
	OUT_ENDPOINT,
}
