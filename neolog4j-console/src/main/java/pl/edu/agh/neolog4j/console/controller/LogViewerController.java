package pl.edu.agh.neolog4j.console.controller;

import java.beans.PropertyEditorSupport;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.neo4j.fieldaccess.Neo4jConversionServiceFactoryBean.StringToEnumConverterFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import pl.edu.agh.neolog4j.console.dao.DatabaseNodeDao;
import pl.edu.agh.neolog4j.console.model.AjaxResponse;
import pl.edu.agh.neolog4j.console.model.DatabaseNode;
import pl.edu.agh.neolog4j.console.model.LogPathNode;
import pl.edu.agh.neolog4j.console.model.NeoLogEventRow;
import pl.edu.agh.neolog4j.console.service.LogEventService;
import pl.edu.agh.neolog4j.dao.ApplicationDao;
import pl.edu.agh.neolog4j.dao.query.LogEventField;
import pl.edu.agh.neolog4j.dao.query.LogEventQuery;
import pl.edu.agh.neolog4j.dao.query.LogLevel;
import pl.edu.agh.neolog4j.dao.query.OrderType;
import pl.edu.agh.neolog4j.model.Application;
import pl.edu.agh.neolog4j.model.ApplicationInstance;

@Controller
@RequestMapping("/logviewer")
public class LogViewerController {
	@Autowired
	private LogEventService logEventService;
	@Autowired
	private ApplicationDao applicationDao;
	@Autowired
	private DatabaseNodeDao databaseNodeDao;
	
	
	@RequestMapping(method = RequestMethod.GET)
	public String index(@ModelAttribute("query") LogEventQuery query, @RequestParam(defaultValue="0") boolean f, ModelMap model){
		List<NeoLogEventRow> neoLogEventsRows = null;
		if(f){
			if(query.getApplicationInstances() == null){
				query.setApplicationInstances(new HashSet<ApplicationInstance>());
			}
			if(query.getLevels() == null){
				query.setLevels(new HashSet<LogLevel>());
			}
			neoLogEventsRows = logEventService.getByQuery(query);
		}
		else{
			query = new LogEventQuery();
			query.addLevel(LogLevel.DEBUG);
			query.addLevel(LogLevel.INFO);
			query.addLevel(LogLevel.WARNING);
			query.addLevel(LogLevel.ERROR);
			query.orderBy(LogEventField.DATE, OrderType.ASC);
			query.offset(0);
			query.limit(100);
			neoLogEventsRows = new ArrayList<NeoLogEventRow>();
		}
		model.addAttribute("f", f);
		model.addAttribute("query", query);
		model.addAttribute("neoLogEventRows", neoLogEventsRows);
		return "logviewer";
	}
	
	@RequestMapping(value="/ajax/logpath/new", method = RequestMethod.GET)
	public @ResponseBody AjaxResponse newLogPath(
			@RequestParam String eventId, @RequestParam String databaseId){
		long eventGraphId = Long.parseLong(eventId);
		long databaseGraphId = Long.parseLong(databaseId);
		DatabaseNode database = databaseNodeDao.get(databaseGraphId);
		AjaxResponse response = new AjaxResponse();

		try{
			LogPathNode logPathNode = logEventService.getNewLogPath(eventGraphId, database);
			response.setResult(logPathNode);
			response.setStatus(AjaxResponse.OK);
		}
		catch(Exception e){
			e.printStackTrace();
			response.setError("Server error: " + e.getMessage());
			response.setStatus(AjaxResponse.ERROR);
		}
		
		return response;
	}
	
	@RequestMapping(value="/ajax/logpath/next", method = RequestMethod.GET)
	public @ResponseBody AjaxResponse loadNextLogPath(
			@RequestParam String eventId, @RequestParam String databaseUrl,
			@RequestParam(defaultValue="false") boolean proposals){
		
		long eventGraphId = Long.parseLong(eventId);
		AjaxResponse response = new AjaxResponse();
		try{
			LogPathNode logPathNode = logEventService.getNewLogPath(eventGraphId, databaseUrl, 0, 4, proposals);
			response.setResult(logPathNode);
			response.setStatus(AjaxResponse.OK);
		}
		catch(Exception e){
			e.printStackTrace();
			response.setError("Server error: " + e.getMessage());
			response.setStatus(AjaxResponse.ERROR);
		}
		
		return response;
	}

	@RequestMapping(value="/ajax/logpath/previous", method = RequestMethod.GET)
	public @ResponseBody AjaxResponse loadPreviosLogPath(
			@RequestParam String eventId, @RequestParam String databaseUrl,
			@RequestParam(defaultValue="false") boolean proposals){
		long eventGraphId = Long.parseLong(eventId);
		AjaxResponse response = new AjaxResponse();

		try{
			LogPathNode logPathNode = logEventService.getNewLogPath(eventGraphId, databaseUrl, 4, 0, proposals);
			response.setResult(logPathNode);
			response.setStatus(AjaxResponse.OK);
		}
		catch(Exception e){
			e.printStackTrace();
			response.setError("Server error: " + e.getMessage());
			response.setStatus(AjaxResponse.ERROR);
		}
		
		return response;
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		StringToEnumConverterFactory converterFactory = new StringToEnumConverterFactory();
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	    
		binder.registerCustomEditor(ApplicationInstance.class, new PropertyEditorSupport() {
	        @Override
	        public void setAsText(String text) {
	        	long graphId = Long.parseLong(text);
	        	ApplicationInstance applicationInstance = applicationDao.getApplicationInstance(graphId);
	            setValue(applicationInstance);
	        }
	    });
	    
		final Converter<String, LogLevel> logLevelConverter = 
			converterFactory.getConverter(LogLevel.class);
		binder.registerCustomEditor(LogLevel.class, new PropertyEditorSupport() {
	        @Override
	        public void setAsText(String text) {
	            setValue(logLevelConverter.convert(text));
	        }
	    });
	    
		final Converter<String, LogEventField> logEventFieldConverter = 
			converterFactory.getConverter(LogEventField.class);
		binder.registerCustomEditor(LogEventField.class, new PropertyEditorSupport() {
	        @Override
	        public void setAsText(String text) {
	            setValue(logEventFieldConverter.convert(text));
	        }
	    });
	    
		final Converter<String, OrderType> orderTypeConverter = 
			converterFactory.getConverter(OrderType.class);
		binder.registerCustomEditor(OrderType.class, new PropertyEditorSupport() {
	        @Override
	        public void setAsText(String text) {
	            setValue(orderTypeConverter.convert(text));
	        }
	    });
	}
	
	@ModelAttribute("logLevels")
	public List<LogLevel> populateLogLevels() {
 		List<LogLevel> logLevels = new ArrayList<LogLevel>();
		logLevels.add(LogLevel.TRACE);
		logLevels.add(LogLevel.DEBUG);
		logLevels.add(LogLevel.INFO);
		logLevels.add(LogLevel.WARNING);
		logLevels.add(LogLevel.ERROR);
		return logLevels;
	}
	
	@ModelAttribute("applicationInstances")
	public List<ApplicationInstance> populateApplications() {
 		List<ApplicationInstance> applicationInstanceList = new ArrayList<ApplicationInstance>();
 		List<Application> applications = new ArrayList<Application>(applicationDao.getApplications());
 		for(Application application: applications){
 			applicationInstanceList.addAll(this.applicationDao.getAllInstances(application));
 		}
 		Collections.sort(applicationInstanceList);
		return applicationInstanceList;
	}
	
	@ModelAttribute("orderFields")
	public List<String> populateOrderFields() {
 		List<String> orderFieldList = new ArrayList<String>();
 		orderFieldList.add(LogEventField.DATE.name());
		return orderFieldList;
	}
	
	@ModelAttribute("orderTypes")
	public List<String> populateOrderTypes() {
 		List<String> orderTypeList = new ArrayList<String>();
 		orderTypeList.add(OrderType.ASC.name());
 		orderTypeList.add(OrderType.DESC.name());
		return orderTypeList;
	}
}
