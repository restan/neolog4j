package pl.edu.agh.neolog4j.console.model;


public class AjaxResponse {
	public static final String OK = "OK";
	public static final String ERROR = "ERROR";
	private String status;
	private String error;
	private Object result;
	
	public String getError() {
		return error;
	}
	
	public void setError(final String error) {
		this.error = error;
	}

	public Object getResult() {
		return result;
	}
	
	public void setResult(final Object result) {
		this.result = result;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}
}
