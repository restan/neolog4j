package pl.edu.agh.neolog4j.console.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.edu.agh.neolog4j.dao.EndpointDao;
import pl.edu.agh.neolog4j.model.InEndpoint;
import pl.edu.agh.neolog4j.model.OutEndpoint;

@Service
public class EndpointService {
	
	@Autowired
	protected EndpointDao endpointDao;
	
	public void connectEndpoints(OutEndpoint outEndpoint, InEndpoint inEndpoint){
		this.endpointDao.connect(inEndpoint, outEndpoint);
	}

	public void disconnectEndpoints(OutEndpoint outEp, InEndpoint inEp) {
		this.endpointDao.disconnectEndpoints(inEp, outEp);
	}

	public OutEndpoint getOutEndpoint(String application, String endpoint) {
		return this.endpointDao.getOutEndpoint(endpoint, application);
	}
	
	public InEndpoint getInEndpoint(String application, String endpoint) {
		return this.endpointDao.getInEndpoint(endpoint, application);
	}
}
