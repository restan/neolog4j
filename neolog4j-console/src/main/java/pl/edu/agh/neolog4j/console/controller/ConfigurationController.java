package pl.edu.agh.neolog4j.console.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import pl.edu.agh.neolog4j.console.model.AjaxResponse;
import pl.edu.agh.neolog4j.console.model.DatabaseConnectionError;
import pl.edu.agh.neolog4j.console.model.DatabaseExistException;
import pl.edu.agh.neolog4j.console.model.DatabaseNode;
import pl.edu.agh.neolog4j.console.service.DatabaseNodeService;
import pl.edu.agh.neolog4j.console.service.EndpointService;
import pl.edu.agh.neolog4j.console.utils.graph.ConfigurationGraphBuilder;
import pl.edu.agh.neolog4j.console.utils.graph.GraphWriter;
import pl.edu.agh.neolog4j.dao.DaoFactory;
import pl.edu.agh.neolog4j.model.InEndpoint;
import pl.edu.agh.neolog4j.model.OutEndpoint;

@Controller
@RequestMapping("/configuration")
public class ConfigurationController {
	@Autowired
	private DatabaseNodeService databaseService;
	@Autowired
	private EndpointService endpointService;
	@Autowired
	private DaoFactory daoFactory;

	@RequestMapping(method = RequestMethod.GET)
	public final String index(final ModelMap model) {
		List<DatabaseNode> databases =
				new ArrayList<DatabaseNode>(databaseService.getDatabases());
		Collections.sort(databases);
		model.addAttribute("databases", databases);
		return "configuration";
	}

	@RequestMapping(value = "/ajax/addDatabase", method = RequestMethod.POST)
	@ResponseBody
	public final AjaxResponse ajaxAddDatabase(
			@RequestParam final String url,
			@RequestParam final boolean testConnection) throws IOException {
		AjaxResponse response = new AjaxResponse();
		try {
			DatabaseNode database =
					databaseService.addDatabase(url, testConnection);
			response.setResult(database);
			response.setStatus(AjaxResponse.OK);
		} catch (DatabaseExistException e) {
			response.setError("Database already exist");
			response.setStatus(AjaxResponse.ERROR);
		} catch (DatabaseConnectionError e) {
			response.setError("Cannot connect to database");
			response.setStatus(AjaxResponse.ERROR);
		} catch (Exception e) {
			e.printStackTrace();
			response.setError("Server error: " + e.getMessage());
			response.setStatus(AjaxResponse.ERROR);
		}
		return response;
	}

	@RequestMapping(value = "/ajax/deleteDatabase", method = RequestMethod.POST)
	@ResponseBody
	public final AjaxResponse ajaxDeleteDatabase(
			@RequestParam final String url) {
		AjaxResponse response = new AjaxResponse();
		try {
			DatabaseNode database = databaseService.getDatabase(url);
			databaseService.deleteDatabase(database);
			response.setStatus(AjaxResponse.OK);
		} catch (Exception e) {
			e.printStackTrace();
			response.setError("Server error: " + e.getMessage());
			response.setStatus(AjaxResponse.ERROR);
		}
		return response;
	}

	@RequestMapping(
		value = "/ajax/synchronizeDatabase",
		method = RequestMethod.POST
	)
	@ResponseBody
	public final AjaxResponse synchronizeDatabase(
			@RequestParam final String url) {
		AjaxResponse response = new AjaxResponse();
		try {
			DatabaseNode database = databaseService.getDatabase(url);
			if (database == null) {
				response.setError("Database not exist");
				response.setStatus(AjaxResponse.ERROR);
			} else {
				databaseService.synchroniseDatabase(database);
				response.setResult(database);
				response.setStatus(AjaxResponse.OK);
			}
		} catch (DatabaseConnectionError e) {
			response.setError("Cannot connect to database");
			response.setStatus(AjaxResponse.ERROR);
		} catch (Exception e) {
			e.printStackTrace();
			response.setError("Server error: " + e.getMessage());
			response.setStatus(AjaxResponse.ERROR);
		}
		return response;
	}

	@RequestMapping(
		value = "/ajax/connectEndpoints",
		method = RequestMethod.POST
	)
	@ResponseBody 
	public final AjaxResponse ajaxConnectEndpoints(
			@RequestParam final String outApplication,
			@RequestParam final String outEndpoint,
			@RequestParam final String inApplication,
			@RequestParam final String inEndpoint
			) {
		OutEndpoint outEp =
				endpointService.getOutEndpoint(outApplication, outEndpoint);
		InEndpoint inEp =
				endpointService.getInEndpoint(inApplication, inEndpoint);

		AjaxResponse response = new AjaxResponse();
		try {
			endpointService.connectEndpoints(outEp, inEp);
			response.setStatus(AjaxResponse.OK);
		} catch (Exception e) {
			e.printStackTrace();
			response.setError("Server error: " + e.getMessage());
			response.setStatus(AjaxResponse.ERROR);
		}
		return response;
	}

	@RequestMapping(
		value = "/ajax/disconnectEndpoints",
		method = RequestMethod.POST
	)
	@ResponseBody
	public final AjaxResponse ajaxDisonnectEndpoints(
			@RequestParam final String outApplication,
			@RequestParam final String outEndpoint,
			@RequestParam final String inApplication,
			@RequestParam final String inEndpoint) {
		OutEndpoint outEp =
				endpointService.getOutEndpoint(outApplication, outEndpoint);
		InEndpoint inEp =
				endpointService.getInEndpoint(inApplication, inEndpoint);

		AjaxResponse response = new AjaxResponse();

		try {
			endpointService.disconnectEndpoints(outEp, inEp);
			response.setStatus(AjaxResponse.OK);
		} catch (Exception e) {
			e.printStackTrace();
			response.setError("Server error: " + e.getMessage());
			response.setStatus(AjaxResponse.ERROR);
		}
		return response;
	}

	@RequestMapping(
		value = "/ajax/configurationGraph",
		method = RequestMethod.GET
	)
	@ResponseBody
	public final AjaxResponse configurationGraph() {
		AjaxResponse response = new AjaxResponse();

		try {
			Set<DatabaseNode> databases = databaseService.getDatabases();
			ConfigurationGraphBuilder graphBuilder =
					new ConfigurationGraphBuilder(daoFactory);
			GraphWriter writer = new GraphWriter();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			writer.write(graphBuilder.create(databases), baos);
			String output = new String(baos.toByteArray());
			response.setResult(output);
			response.setStatus(AjaxResponse.OK);
		} catch (Exception e) {
			e.printStackTrace();
			response.setError("Server error: " + e.getMessage());
			response.setStatus(AjaxResponse.ERROR);
		}
		return response;
	}

	public final DatabaseNodeService getDatabaseService() {
		return databaseService;
	}

	public final void setDatabaseService(
			final DatabaseNodeService databaseService) {
		this.databaseService = databaseService;
	}

	public final EndpointService getEndpointService() {
		return endpointService;
	}

	public final void setEndpointService(
			final EndpointService endpointService) {
		this.endpointService = endpointService;
	}
}
