package pl.edu.agh.neolog4j.console.utils.graph;

import it.uniroma1.dis.wsngroup.gexf4j.core.Gexf;
import it.uniroma1.dis.wsngroup.gexf4j.core.impl.StaxGraphWriter;

import java.io.IOException;
import java.io.OutputStream;

public class GraphWriter {
	public final void write(final Gexf gexf, final OutputStream out)
			throws IOException {
		StaxGraphWriter graphWriter = new StaxGraphWriter();
		graphWriter.writeToStream(gexf, out, "UTF-8");
	}
}
