package pl.edu.agh.neolog4j.console.model;

import java.util.Comparator;

import pl.edu.agh.neolog4j.dao.query.LogEventField;

public class NeoLogEventRowComparator implements Comparator<NeoLogEventRow>{

	
	protected LogEventField compareField = null;
	
	public NeoLogEventRowComparator(LogEventField p_field) {
		this.compareField = p_field;
	}
	
	public int compare(NeoLogEventRow row1, NeoLogEventRow row2) {

		switch (this.compareField) {
		case DATE:
			return (int) (row1.getNeoLogEvent().getTimestamp() - row2.getNeoLogEvent().getTimestamp());
		default:
			break;
		}
		return 0;
	}

}
