package pl.edu.agh.neolog4j.console.model;

import pl.edu.agh.neolog4j.model.NeoLogEvent;

public class NeoLogEventRow {
	private NeoLogEvent neoLogEvent;
	private DatabaseNode databaseNode;

	public NeoLogEvent getNeoLogEvent() {
		return neoLogEvent;
	}
	public void setNeoLogEvent(NeoLogEvent neoLogEvent) {
		this.neoLogEvent = neoLogEvent;
	}
	public DatabaseNode getDatabaseNode() {
		return databaseNode;
	}
	public void setDatabaseNode(DatabaseNode databaseNode) {
		this.databaseNode = databaseNode;
	}
}
