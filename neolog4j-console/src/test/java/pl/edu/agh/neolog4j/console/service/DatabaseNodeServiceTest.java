package pl.edu.agh.neolog4j.console.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import pl.edu.agh.neolog4j.AbstractTest;
import pl.edu.agh.neolog4j.console.model.DatabaseConnectionError;
import pl.edu.agh.neolog4j.console.model.DatabaseExistException;
import pl.edu.agh.neolog4j.console.model.DatabaseNode;
import pl.edu.agh.neolog4j.dao.ApplicationDao;
import pl.edu.agh.neolog4j.dao.DaoFactory;
import pl.edu.agh.neolog4j.model.Application;
import pl.edu.agh.neolog4j.model.ApplicationInstance;
import pl.edu.agh.neolog4j.model.Endpoint;
import pl.edu.agh.neolog4j.model.InEndpoint;
import pl.edu.agh.neolog4j.model.OutEndpoint;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:META-INF/spring/test-applicationContext.xml")
public class DatabaseNodeServiceTest extends AbstractTest {

	protected static final String TEST_APP_NAME = "testAppName";
	protected static final String TEST_URL = "testUrl";
	protected static final String IN_ENDPOINT_1 = "InEndpoint1";
	protected static final String IN_ENDPOINT_2 = "InEndpoint2";
	protected static final String OUT_ENDPOINT_1 = "OutEndpoint1";
	protected static final String OUT_ENDPOINT_2 = "OutEndpoint2";
	
	
	@Autowired
	protected DatabaseNodeService service;
	
	@Autowired
	protected ApplicationDao applicationDao;
	
	@Mock
    private DaoFactory remoteDb;
	
	@Mock
	private ApplicationDao remoteApplicationDao;
	
	protected Set<ApplicationInstance> mockedInstances = getMockedInstances();
	protected Set<Endpoint> mockedEndpoints = getMockedEndpoints1();
	
	
	
	@Before
	public void setUp() throws Exception {
		this.remoteDb = Mockito.mock(DaoFactory.class);
		this.remoteApplicationDao = Mockito.mock(ApplicationDao.class);
		when(this.remoteDb.getApplicationDao()).thenReturn(this.remoteApplicationDao);
		Set<Application> applications = new HashSet<Application>();
		Application testApplication = Mockito.mock(Application.class);
		applications.add(testApplication);
		when(testApplication.getName()).thenReturn(TEST_APP_NAME);
		when(this.remoteApplicationDao.getAllNodes(Application.class)).thenReturn(applications);
		when(this.remoteApplicationDao.getInstances(TEST_APP_NAME, null)).thenReturn(this.mockedInstances);
		when(this.remoteApplicationDao.getInstances(eq(TEST_APP_NAME), any(Date.class))).thenReturn(this.mockedInstances);
		when(this.remoteApplicationDao.getApplicationEndpoints(testApplication)).thenReturn(this.mockedEndpoints);

	}
	
	protected Set<ApplicationInstance> getMockedInstances() {
		
		Set<ApplicationInstance> result = new HashSet<ApplicationInstance>();
		for (int i = 0; i < 10; i++) {
			ApplicationInstance instance = Mockito.mock(ApplicationInstance.class);
			when(instance.getCreateDate()).thenReturn(new Date());
			result.add(instance);
		}
		return result;
	}
	
	protected Set<Endpoint> getMockedEndpoints1() {
		Set<Endpoint> result = new HashSet<Endpoint>();
		InEndpoint mockIn = Mockito.mock(InEndpoint.class);
		when(mockIn.getName()).thenReturn(IN_ENDPOINT_1);
		OutEndpoint mockOut = Mockito.mock(OutEndpoint.class);
		when(mockOut.getName()).thenReturn(OUT_ENDPOINT_1);
		result.add(mockOut);
		result.add(mockIn);
		return result;
	}
	
	protected Set<Endpoint> getMockedEndpoints2() {
		Set<Endpoint> result = new HashSet<Endpoint>();
		InEndpoint mockIn = Mockito.mock(InEndpoint.class);
		when(mockIn.getName()).thenReturn(IN_ENDPOINT_2);
		OutEndpoint mockOut = Mockito.mock(OutEndpoint.class);
		when(mockOut.getName()).thenReturn(OUT_ENDPOINT_2);
		result.add(mockOut);
		result.add(mockIn);
		return result;
	}
	
	@Test
	public void testSynchroniseDatabase() throws DatabaseConnectionError, DatabaseExistException {
		DatabaseNode database = this.service.addDatabase(TEST_URL, false);
		Set<Application> allApplications = this.applicationDao.getAllNodes(Application.class);
		Set<ApplicationInstance> allInstances = this.applicationDao.getAllNodes(ApplicationInstance.class);
		Set<InEndpoint> inNodes = this.applicationDao.getAllNodes(InEndpoint.class);
		Set<OutEndpoint> outNodes = this.applicationDao.getAllNodes(OutEndpoint.class);
		assertEquals(0, allApplications.size());
		assertEquals(0, allInstances.size());
		assertEquals(0, inNodes.size());
		assertEquals(0, outNodes.size());
		this.service.synchroniseDatabase(this.remoteDb, database);
		allApplications = this.applicationDao.getAllNodes(Application.class);
		allInstances = this.applicationDao.getAllNodes(ApplicationInstance.class);
		inNodes = this.applicationDao.getAllNodes(InEndpoint.class);
		outNodes = this.applicationDao.getAllNodes(OutEndpoint.class);
		assertEquals(1, allApplications.size());
		assertEquals(10, allInstances.size());
		assertEquals(1, inNodes.size());
		assertEquals(1, outNodes.size());
		this.mockedInstances = getMockedInstances();
		this.mockedEndpoints.addAll(getMockedEndpoints2());
		this.service.synchroniseDatabase(this.remoteDb, database);
		allApplications = this.applicationDao.getAllNodes(Application.class);
		allInstances = this.applicationDao.getAllNodes(ApplicationInstance.class);
		inNodes = this.applicationDao.getAllNodes(InEndpoint.class);
		outNodes = this.applicationDao.getAllNodes(OutEndpoint.class);
		assertEquals(1, allApplications.size());
		assertEquals(20, allInstances.size());
		assertEquals(2, inNodes.size());
		assertEquals(2, outNodes.size());
	}
}
