package pl.edu.agh.neolog4j.console.dao;

import static org.junit.Assert.*;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.neo4j.graphdb.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import pl.edu.agh.neolog4j.console.model.DatabaseExistException;
import pl.edu.agh.neolog4j.console.model.DatabaseNode;
import pl.edu.agh.neolog4j.dao.AbstractDao;
import pl.edu.agh.neolog4j.dao.AbstractNamedEntitiesDaoTest;
import pl.edu.agh.neolog4j.dao.ApplicationDao;
import pl.edu.agh.neolog4j.model.ApplicationInstance;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:META-INF/spring/test-applicationContext.xml")
public class DatabseNodeDaoTest extends AbstractNamedEntitiesDaoTest<DatabaseNode>{

	protected static final String TEST_URL = "www.db.test";
	
	@Autowired
	protected DatabaseNodeDao dao;
	
	@Autowired
	protected ApplicationDao applicationDao;
	
	
	@Override
	protected DatabaseNode getNode(String p_name) {
		return dao.getDatabase(p_name);
	}

	@Override
	protected Class<DatabaseNode> getProcessedEntityClass() {
		return DatabaseNode.class;
	}

	@Override
	protected AbstractDao getAbstractDao() {
		return this.dao;
	}
	
	
	@Test
	public void testGetDatabases() {
		Set<DatabaseNode> databases = new HashSet<DatabaseNode>();
		databases.add(this.dao.getDatabase(TEST_URL));
		databases.add(this.dao.getDatabase(TEST_URL));
		databases.add(this.dao.getDatabase(TEST_URL + "_2"));
		databases.add(this.dao.getDatabase(TEST_URL + "_3"));
		Set<DatabaseNode> resultDatabases = dao.getDatabases();
		assertEquals(databases, resultDatabases);
		
		
	}
	
	
	@Test(expected = DatabaseExistException.class)
	public void testAddDatabase() throws DatabaseExistException {
		DatabaseNode addDatabase = this.dao.addDatabase(TEST_URL);
		DatabaseNode database = this.dao.getDatabase(TEST_URL);
		assertEquals(addDatabase, database);
		this.dao.addDatabase(TEST_URL);
	}
	
	@Test
	public void testGetDatabase() {
		DatabaseNode database = this.dao.getDatabase(TEST_URL);
		DatabaseNode database2 = this.dao.getDatabase(TEST_URL);
		DatabaseNode database3 = this.dao.getDatabase(TEST_NAME);
		assertEquals(database, database2);
		assertNotEquals(database, database3);
	}
	
	@Test(expected = DataRetrievalFailureException.class)
	public void testGet() {
		DatabaseNode database = this.dao.getDatabase(TEST_URL);
		DatabaseNode databaseNode = this.dao.get(database.getGraphID());
		assertEquals(database, databaseNode);
		this.dao.get(123123);
	}
	
	@Test(expected = NotFoundException.class)
	public void testDeleteDatabase() throws DatabaseExistException {
		DatabaseNode database1 = this.dao.addDatabase(TEST_URL);
		DatabaseNode database2 = this.dao.addDatabase(TEST_NAME);
		assertEquals(2, this.dao.getDatabases().size());
		this.dao.deleteDatabase(database1);
		assertEquals(1, this.dao.getDatabases().size());
		this.dao.deleteDatabase(database2);
		assertEquals(0, this.dao.getDatabases().size());
		this.dao.deleteDatabase(database1);
	}
	
	@Test
	public void testAddApplicationInstance() {
		DatabaseNode database = this.dao.getDatabase(TEST_URL);
		ApplicationInstance applicationInstance = this.applicationDao.getApplicationInstance(TEST_NAME);
		Set<ApplicationInstance> instances = new HashSet<ApplicationInstance>();
		instances.add(applicationInstance);
		this.dao.addApplicationInstance(database, applicationInstance);
		DatabaseNode requery = this.dao.requery(database, DatabaseNode.class);
		Set<ApplicationInstance> applicationInstances = requery.getApplicationInstances();
		assertEquals(instances, applicationInstances);
	}



}
