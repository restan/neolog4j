package pl.edu.agh.neolog4j.logging;

import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttr;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.PatternLayout;

import pl.edu.agh.neolog4j.dao.ApplicationDao;
import pl.edu.agh.neolog4j.dao.DaoFactory;
import pl.edu.agh.neolog4j.dao.EndpointDao;
import pl.edu.agh.neolog4j.dao.NeoLogEventDao;
import pl.edu.agh.neolog4j.model.ApplicationInstance;
import pl.edu.agh.neolog4j.model.NeoLogEvent;
import pl.edu.agh.neolog4j.model.SimpleLogEvent;

@Plugin(name = "NeoLog", type = "Core", elementType = "appender")
public final class NeoLogAppender extends AbstractAppender<String> {
	
	private NeoLogEventDao neoLogEventDao;
	private ApplicationInstance applicationInstance;
	
	private NeoLogAppender(
			final String name,
			final Filter filter,
			final Layout<String> layout,
			final boolean handleException,
			final NeoLogEventDao neoLogEventDao,
			final ApplicationInstance applicationInstance) {
		super(name, filter, layout, handleException);
		this.neoLogEventDao = neoLogEventDao;
		this.applicationInstance = applicationInstance;
	}

	@PluginFactory
    public static NeoLogAppender createAppender(
    		@PluginAttr("name") final String name,
    		@PluginAttr("appName") final String appName,
    		@PluginAttr("suppressExceptions") final String suppress,
    		@PluginElement("layout") final Layout<String> layout,
    		@PluginElement("filters") final Filter filter,
    		@PluginAttr("dataBaseUrl") final String url,
    		@PluginAttr("dataBaseUser") final String userName,
    		@PluginAttr("dataBasePasswd") final String passwd) {
		boolean handleExceptions =
				suppress == null ? true : Boolean.valueOf(suppress);
		DaoFactory daoFactory =
				DaoFactory.getInstance(url, userName, passwd);
		NeoLogEventDao neoLogEventDao = daoFactory.getNeoLogEventDao();
		ApplicationDao applicationDao = daoFactory.getApplicationDao();
		ApplicationInstance applicationInstance =
				applicationDao.getApplicationInstance(appName);
		EndpointDao endpointDao = daoFactory.getEndpointDao();
		InEndpointRegistry.init(endpointDao, appName);
		OutEndpointRegistry.init(endpointDao, appName);
		NeoLogContext.init(neoLogEventDao);
		
		Layout<String> finalLayout = layout;
		if (finalLayout == null) {
			finalLayout = PatternLayout.createLayout(null, null, null, null);
        }
		return new NeoLogAppender(name, filter, finalLayout, handleExceptions,
				neoLogEventDao, applicationInstance);
	}
	
	public void append(final LogEvent event) {
		SimpleLogEvent simpleLogEvent = getSimpleLogEvent(event);
		NeoLogEvent neoLogEvent =
				neoLogEventDao.getNeoLogEvent(simpleLogEvent,
						applicationInstance);
		NeoLogContext.connectNeoLogEvent(neoLogEvent);
	}
	
	private SimpleLogEvent getSimpleLogEvent(final LogEvent event) {
		SimpleLogEvent simpleLogEvent = new SimpleLogEvent();
		simpleLogEvent.setMessage(getLayout().toSerializable(event));
		simpleLogEvent.setLevel(event.getLevel().toString());
		simpleLogEvent.setLoggerName(event.getLoggerName());
		simpleLogEvent.setStackTraceElement(event.getSource());
		simpleLogEvent.setThreadName(event.getThreadName());
		simpleLogEvent.setTimestamp(event.getMillis());
		return simpleLogEvent;
	}
}
