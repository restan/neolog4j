package pl.edu.agh.neolog4j.logging;

import pl.edu.agh.neolog4j.dao.EndpointDao;
import pl.edu.agh.neolog4j.model.OutEndpoint;

public final class OutEndpointRegistry {
	private static EndpointDao endpointDao;
	private static String application;
	
	public static OutEndpoint register(final String name) {
		return endpointDao.getOutEndpoint(name, application);
	}
	
	public static void init(
			final EndpointDao endpointDao, 
			final String application) {
		OutEndpointRegistry.endpointDao = endpointDao;
		OutEndpointRegistry.application = application;
	}
	
	private OutEndpointRegistry() { }
}
