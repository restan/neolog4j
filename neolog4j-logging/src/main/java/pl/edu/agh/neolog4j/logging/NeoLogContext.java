package pl.edu.agh.neolog4j.logging;

import pl.edu.agh.neolog4j.dao.NeoLogEventDao;
import pl.edu.agh.neolog4j.model.InEndpoint;
import pl.edu.agh.neolog4j.model.NeoLogEvent;
import pl.edu.agh.neolog4j.model.OutEndpoint;

public final class NeoLogContext {
	private static InheritableThreadLocal<NeoLogEvent> lastNeoLogEvent =
			new InheritableThreadLocal<NeoLogEvent>();
	private static ThreadLocal<InEndpoint> lastInEndpoint =
			new ThreadLocal<InEndpoint>();
	private static NeoLogEventDao neoLogEventDao;
	
	public static void init(final NeoLogEventDao neoLogEventDao) {
		NeoLogContext.neoLogEventDao = neoLogEventDao;
		NeoLogContext.setLastNeoLogEvent(null);
		NeoLogContext.setInEndpoint(null);
	}
	
	public static void connectNeoLogEvent(final NeoLogEvent logEvent) {
		NeoLogEvent last = NeoLogContext.getLastNeoLogEvent();
		if (last != null) {
			logEvent.setPrevious(last);
		} else {
			InEndpoint inEndpoint = NeoLogContext.getInEndpoint();
			logEvent.setSourceEndpoint(inEndpoint);
		}
		neoLogEventDao.save(logEvent);
		NeoLogContext.setLastNeoLogEvent(logEvent);
	}

	public static void processInEndpoint(final InEndpoint endpoint) {
		NeoLogContext.setLastNeoLogEvent(null);
		NeoLogContext.setInEndpoint(endpoint);
	}

	public static void processOutEndpoint(final OutEndpoint endpoint) {
		NeoLogEvent logEvent = NeoLogContext.getLastNeoLogEvent();
		if (logEvent != null) {
			logEvent.setDestinationEndpoint(endpoint);
			neoLogEventDao.save(logEvent);
		}
	}

	private static NeoLogEvent getLastNeoLogEvent() {
		return NeoLogContext.lastNeoLogEvent.get();
	}

	private static void setLastNeoLogEvent(final NeoLogEvent lastNeoLogEvent) {
		NeoLogContext.lastNeoLogEvent.set(lastNeoLogEvent);
	}
	
	private static InEndpoint getInEndpoint() {
		return NeoLogContext.lastInEndpoint.get();
	}

	private static void setInEndpoint(final InEndpoint inEndpoint) {
		NeoLogContext.lastInEndpoint.set(inEndpoint);
	}
	
	private NeoLogContext() { }
}
