package pl.edu.agh.neolog4j.logging;

import pl.edu.agh.neolog4j.dao.EndpointDao;
import pl.edu.agh.neolog4j.model.InEndpoint;

public final class InEndpointRegistry {
	private static EndpointDao endpointDao;
	private static String application;
	
	public static InEndpoint register(final String name) {
		return endpointDao.getInEndpoint(name, application);
	}
	
	public static void init(
			final EndpointDao endpointDao,
			final String application) {
		InEndpointRegistry.endpointDao = endpointDao;
		InEndpointRegistry.application = application;
	}
	
	private InEndpointRegistry() { }
}
