package pl.edu.agh.neolog4j.aspect;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

import pl.edu.agh.neolog4j.logging.NeoLogContext;
import pl.edu.agh.neolog4j.logging.OutEndpointRegistry;
import pl.edu.agh.neolog4j.model.OutEndpoint;

@Aspect
public abstract class OutEndpointAspect{
	private static final String namePattern = "(\\w+\\.)*(.+)";
	
	@Pointcut
	public void beforeOutPointcut(){}

	@Pointcut
	public void afterOutPointcut(){}
	
	@Before("beforeOutPointcut()")
	public void _beforeOutPointcut(){
		_outPointcut();
	}

	@After("afterOutPointcut()")
	public void _afterOutPointcut(){
		_outPointcut();
	}
	
	private void _outPointcut(){
		NeoLogContext.processOutEndpoint(this.endpoint);
	}
	
	private OutEndpoint endpoint = null;
		
	public OutEndpointAspect(){
		String endpointName = this.getClass().getName().replaceAll(OutEndpointAspect.namePattern, "$2");
		this.endpoint = OutEndpointRegistry.register(endpointName);
	}
}
