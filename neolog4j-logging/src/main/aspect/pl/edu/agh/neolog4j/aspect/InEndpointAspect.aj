package pl.edu.agh.neolog4j.aspect;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

import pl.edu.agh.neolog4j.logging.InEndpointRegistry;
import pl.edu.agh.neolog4j.logging.NeoLogContext;
import pl.edu.agh.neolog4j.model.InEndpoint;

@Aspect
public abstract class InEndpointAspect {
	private static final String namePattern = "(\\w+\\.)*(.+)";
	
	@Pointcut
	public void beforeInPointcut(){}

	@Pointcut
	public void afterInPointcut(){}
	
	@Before("beforeInPointcut()")
	public void _beforeInPointcut(){
		_inPointcut();
	}

	@After("afterInPointcut()")
	public void _afterInPointcut(){
		_inPointcut();
	}
	
	private void _inPointcut(){
		NeoLogContext.processInEndpoint(this.endpoint);
	}
	
	private InEndpoint endpoint = null;
    
	public InEndpointAspect(){
		String endpointName = this.getClass().getName().replaceAll(InEndpointAspect.namePattern, "$2");
		this.endpoint = InEndpointRegistry.register(endpointName);
	}
}
