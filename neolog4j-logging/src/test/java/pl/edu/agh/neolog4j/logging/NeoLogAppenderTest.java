package pl.edu.agh.neolog4j.logging;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import pl.edu.agh.neolog4j.dao.ApplicationDao;
import pl.edu.agh.neolog4j.dao.DaoFactory;
import pl.edu.agh.neolog4j.dao.DaoFactory.DatabaseInstanceKey;
import pl.edu.agh.neolog4j.dao.NeoLogEventDao;
import pl.edu.agh.neolog4j.model.ApplicationInstance;
import pl.edu.agh.neolog4j.model.NeoLogEvent;
import pl.edu.agh.neolog4j.model.SimpleLogEvent;

public class NeoLogAppenderTest {
	@Mock
	private DaoFactory daoFactory;
	@Mock
	private ApplicationDao applicationDao;
	@Mock
	private NeoLogEventDao neoLogEventDao;
	@Mock
	private ApplicationInstance applicationInstance;
	
	private String appName = "appName";
	private String dbUrl = "url";
	private String dbUser = "user";
	private String dbPass = "pass";
	
	@Before
	public final void setUp() throws Exception {
		daoFactory = Mockito.mock(DaoFactory.class);
		applicationDao = Mockito.mock(ApplicationDao.class);
		neoLogEventDao = Mockito.mock(NeoLogEventDao.class);
		applicationInstance = Mockito.mock(ApplicationInstance.class);
		Mockito.when(daoFactory.getApplicationDao()).thenReturn(applicationDao);
		Mockito.when(daoFactory.getNeoLogEventDao()).thenReturn(neoLogEventDao);
		Mockito.when(applicationDao.getApplicationInstance(appName))
			.thenReturn(applicationInstance);
		
		Map<DatabaseInstanceKey, DaoFactory> instances =
				new HashMap<DatabaseInstanceKey, DaoFactory>();
		DatabaseInstanceKey databaseInstanceKey =
				new DatabaseInstanceKey(dbUrl, dbUser, dbPass);
		instances.put(databaseInstanceKey, daoFactory);
		Field field = DaoFactory.class.getDeclaredField("instances");
		field.setAccessible(true);
		field.set(new DaoFactory(), instances);
	}
	
	@Test
	@SuppressWarnings("unchecked")
	public final void testCreateAppender() {
		String name = "appender";
		String suppress = "false";
		Layout<String> layout = Mockito.mock(Layout.class);
		Filter filter = Mockito.mock(Filter.class);
		NeoLogAppender appender = NeoLogAppender
				.createAppender(name, appName, suppress, layout,
						filter, dbUrl, dbUser, dbPass);
		assertEquals(appender.getName(), name);
		assertEquals(appender.getFilter(), filter);
		assertEquals(appender.getLayout(), layout);
		
		suppress = null;
		layout = null;
		appender = NeoLogAppender
				.createAppender(name, appName, suppress, layout,
						filter, dbUrl, dbUser, dbPass);
		assertEquals(appender.getName(), name);
		assertEquals(appender.getFilter(), filter);
		assertNotNull(appender.getLayout());
	}
	
	@Test
	@SuppressWarnings("unchecked")
	public final void testAppend() {
		String name = "appender";
		String suppress = "false";
		Layout<String> layout = Mockito.mock(Layout.class);
		Filter filter = Mockito.mock(Filter.class);
		NeoLogAppender appender = NeoLogAppender
				.createAppender(name, appName, suppress, layout,
						filter, dbUrl, dbUser, dbPass);
		LogEvent event = Mockito.mock(LogEvent.class);
		Mockito.when(event.getLevel()).thenReturn(Level.DEBUG);
		NeoLogEvent neoEvent = Mockito.mock(NeoLogEvent.class);
		Mockito.when(neoLogEventDao.getNeoLogEvent(
				Mockito.any(SimpleLogEvent.class),
				Mockito.eq(applicationInstance)))
			.thenReturn(neoEvent);
		appender.append(event);
	}

}

