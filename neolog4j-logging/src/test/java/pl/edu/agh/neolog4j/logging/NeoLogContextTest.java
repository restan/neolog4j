package pl.edu.agh.neolog4j.logging;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import pl.edu.agh.neolog4j.dao.NeoLogEventDao;
import pl.edu.agh.neolog4j.model.InEndpoint;
import pl.edu.agh.neolog4j.model.NeoLogEvent;
import pl.edu.agh.neolog4j.model.OutEndpoint;

public class NeoLogContextTest {
	@Mock
    private NeoLogEventDao neoLogEventDao;

	@Before
	public final void setUp() throws Exception {
		neoLogEventDao = Mockito.mock(NeoLogEventDao.class);
		NeoLogContext.init(neoLogEventDao);
	}
	
	@Test
	public final void testConnectNeoLogEvent() {
		NeoLogEvent logEvent1 = Mockito.mock(NeoLogEvent.class);
		NeoLogContext.connectNeoLogEvent(logEvent1);
		Mockito.verify(logEvent1, Mockito.never())
			.setPrevious(Mockito.any(NeoLogEvent.class));
		Mockito.verify(logEvent1, Mockito.times(1)).setSourceEndpoint(null);
		Mockito.verify(neoLogEventDao, Mockito.times(1)).save(logEvent1);
		
		NeoLogEvent logEvent2 = Mockito.mock(NeoLogEvent.class);
		NeoLogContext.connectNeoLogEvent(logEvent2);
		Mockito.verify(logEvent2, Mockito.times(1)).setPrevious(logEvent1);
		Mockito.verify(logEvent2, Mockito.never())
			.setSourceEndpoint(Mockito.any(InEndpoint.class));
		Mockito.verify(neoLogEventDao, Mockito.times(1)).save(logEvent2);
	}
	
	@Test
	public final void testProcessInEndpoint() {
		NeoLogEvent logEvent1 = Mockito.mock(NeoLogEvent.class);
		NeoLogContext.connectNeoLogEvent(logEvent1);

		InEndpoint inEndpoint = Mockito.mock(InEndpoint.class);
		NeoLogContext.processInEndpoint(inEndpoint);
		
		NeoLogEvent logEvent2 = Mockito.mock(NeoLogEvent.class);
		NeoLogContext.connectNeoLogEvent(logEvent2);
		Mockito.verify(logEvent2, Mockito.never())
			.setPrevious(Mockito.any(NeoLogEvent.class));
		Mockito.verify(logEvent2, Mockito.times(1))
			.setSourceEndpoint(inEndpoint);
		Mockito.verify(neoLogEventDao, Mockito.times(1)).save(logEvent2);
	}
	
	@Test
	public final void testProcessOutEndpoint() {
		OutEndpoint outEndpoint1 = Mockito.mock(OutEndpoint.class);
		NeoLogContext.processOutEndpoint(outEndpoint1);
		Mockito.verify(neoLogEventDao, Mockito.never())
			.save(Mockito.any(NeoLogEvent.class));

		NeoLogEvent logEvent1 = Mockito.mock(NeoLogEvent.class);
		NeoLogContext.connectNeoLogEvent(logEvent1);
		Mockito.reset(neoLogEventDao);
		OutEndpoint outEndpoint2 = Mockito.mock(OutEndpoint.class);
		NeoLogContext.processOutEndpoint(outEndpoint2);
		Mockito.verify(logEvent1, Mockito.times(1))
			.setDestinationEndpoint(outEndpoint2);
		Mockito.verify(neoLogEventDao, Mockito.times(1)).save(logEvent1);
	}

}
