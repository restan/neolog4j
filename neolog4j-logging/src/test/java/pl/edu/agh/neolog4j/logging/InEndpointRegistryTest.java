package pl.edu.agh.neolog4j.logging;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import pl.edu.agh.neolog4j.dao.EndpointDao;
import pl.edu.agh.neolog4j.model.InEndpoint;

public class InEndpointRegistryTest {
	@Mock
    private EndpointDao endpointDao;
	
	@Before
	public final void setUp() throws Exception {
		endpointDao = Mockito.mock(EndpointDao.class);
	}
	
	@Test
	public final void testRegister() {
		String appName = "app";
		String endpointName = "endponint";
		InEndpointRegistry.init(endpointDao, appName);
		InEndpoint endpointMock = Mockito.mock(InEndpoint.class);
		Mockito.when(endpointDao.getInEndpoint(endpointName, appName))
			.thenReturn(endpointMock);
		InEndpoint endpoint = InEndpointRegistry.register(endpointName);
		Mockito.verify(endpointDao, Mockito.times(1))
			.getInEndpoint(endpointName, appName);
		assertEquals(endpointMock, endpoint);
	}
	

	@Test(expected = NullPointerException.class)
	public final void testRegisterFail() {
		InEndpointRegistry.register("endpoint");
	}
}
