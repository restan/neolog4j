package pl.edu.agh.neolog4j.logging;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import pl.edu.agh.neolog4j.dao.EndpointDao;
import pl.edu.agh.neolog4j.model.OutEndpoint;

public class OutEndpointRegistryTest {
	
	@Mock
    private EndpointDao endpointDao;
	
	@Before
	public final void setUp() throws Exception {
		endpointDao = Mockito.mock(EndpointDao.class);
	}
	
	@Test
	public final void testRegister() {
		String appName = "app";
		String endpointName = "endponint";
		OutEndpointRegistry.init(endpointDao, appName);
		OutEndpoint endpointMock = Mockito.mock(OutEndpoint.class);
		Mockito.when(endpointDao.getOutEndpoint(
				endpointName, appName))
			.thenReturn(endpointMock);
		OutEndpoint endpoint = OutEndpointRegistry.register(endpointName);
		Mockito.verify(endpointDao, Mockito.times(1))
			.getOutEndpoint(endpointName, appName);
		assertEquals(endpointMock, endpoint);
	}
	
	@Test(expected = NullPointerException.class)
	public final void testRegisterFail() {
		OutEndpointRegistry.register("endpoint");
	}
}
